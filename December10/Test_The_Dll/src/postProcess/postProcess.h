#ifndef _POST_PROCESS_H
#define _POST_PROCESS_H

#include"..\stroke\stroke.h"
#include<iostream>
#include<errno.h>
#include<sstream>
#include<stack>
#include<windows.h>
#define TEXT_THRES 110
enum stk_state{S,A,AP,V,E,ER};
enum stk_type{SC,SV,LC,RC,MC,LV,RV,MV,W};
struct post_labels
{
	public:
		int index;
		double prob;
};
class Akshara
{
        public:
                string aksharaID;
                vector<vector<int> >stroke_seq;
		vector<string> unicode;
};

class stroke_info
{
	public:
		vector<int>ClassLabel;
		vector<double>ClassProb;
		vector<double>X;
		vector<double>Y;
};

struct stroke_type
{
	string label;
	stk_type type;
	vector<string> unicode;
};

struct comp_stroke
{
	string label;
	stk_type type;
	vector<string> unicode;
	string sub1;
	string sub2;
};
class stroke_state
{
	public:
	int stk_no;
	stk_state state;
	
	stroke_state(int stk_no,stk_state state)
	{
		this->stk_no=stk_no;
		this->state=state;
	}
};
struct stroke_sort{
	public:
		int index;
		double prob;
};

class PostProcess
{
	//for storing all the previous stroke labels of the akshara...
	vector<int>akshara_startp;
	vector<stroke_info>Stk_coll;
	vector<stroke_type> Stk_type_lib;
	vector<comp_stroke> Stk_comp_lib;
	int RECURSION_LIMIT;
	int RECURSION_COUNT;
	int CURRENT_RECURSIVE_COUNT;
        double seg_thresold;
	int total_output;
	int max_size;
	vector<stroke_info> temp_carrier;
	public:
//	void computeAkshara(int , string , double ,  vector <string> &, vector <double> &,int ,vector<int> &);
	
	vector<struct stroke_sort> temp_stroke;
	PostProcess();
	~PostProcess();
	int Process(Stroke &Stk,int Display,vector<vector<string>  >&displaybuff,int topN,string iiit_diction_path,vector<stroke_type> &Stk_type_lib,vector<comp_stroke> &Comp_stk_lib, unsigned int beg_s,unsigned int end_s);
	int ProcessDigit(Stroke &Stk,int Display,vector<vector<string>  >&displaybuff,int topN,string iiit_diction_path,vector<Akshara> &akshara_lib,vector<int> &Stroke_pos_info, unsigned int beg_s,unsigned int end_s);
	int ProcessMixed(Stroke &Stk,int Display,vector<vector<string>  >&displaybuff,int topN,string iiit_diction_path,vector<stroke_type> &Stk_type_lib,vector<comp_stroke> &Comp_stk_lib,vector<Akshara> &akshara_lib,vector<int> &Stroke_pos_info);
	int Recognize(vector<vector<string> >&,int,int,vector<Akshara> &);
	void save_log(vector<vector<string> >&,string);
	void clearall(vector<vector<string> >&);
	int unicode_generate(stack <stroke_state>,vector<string>&);
	vector<string> get_unicode(string);
	stk_type get_stk_type(string);
	void backtrack(unsigned int& curr_stroke, stk_state &curr_seg_state,vector<unsigned int> &Stk_coll_pos,stack<stroke_state> &curr_akshara,bool &tilde_flag, unsigned int end_s);
	//int post_cmp(const void*,const void*);	
	
};
#endif
