/**																
	\file sample1.h
  	\brief  only sample code 
	\details xxxxxxxx
  	\version 1.0.0.0
  	<b> All COPYRIGHTS ARE RESERVERD TO C-DAC GIST PUNE 2012 </b>
*/

#ifndef _SAMPLE1_H_
#define _SAMPLE1_H_

#include <math.h>
#include <stdlib.h>

#ifdef __cplusplus
extern "C"
{  
#endif
		
	//declare functions, verables here see example funtion
	//see for comments http://www.stack.nl/~dimitri/doxygen/docblocks.html

	/** PI
    \brief A macro value of PI
	*/
	#define PI 3.14
		
	/**	typedef struct  myCircle. 
	\brief cicle infrmation
	\details .............
	*/
    typedef struct myCircle
    {
		int  rad;			/**<radius */
		double Area ;		/**<Area of circle */
    }CIRCLE;

	/** fn_CalcAreaCircle
    \brief calculates Area of circle
    \param[in/out] circle info
	*/
	void fn_CalcAreaCircle(CIRCLE* d);

#ifdef __cplusplus
}  
#endif

#endif
