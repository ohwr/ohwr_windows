import pdb
import numpy as np
import csv
type1=[]
type2=[]
type3=[]
bad_words=[]
confusion=np.zeros((144,144))
main_c=0
with open("Classifier_results.txt",'r') as f:
    
    
        
    file_c=0
    count_line=0
    for line in f:
        
        print (main_c)
        if main_c>1000:
            break
        if '*' in line:
            #pdb.set_trace()
            if "*U*" in line:
                break;
            elif ("*S" in line):
                word_c=0
                file_c+=1
                count_line=0
                continue
        if count_line<=2:
           # pdb.set_trace()
            count_line+=1
            continue
        if count_line==3:
           # print (count_line,line)
           # pdb.set_trace()
            count_line+=1
            #pdb.set_trace()
            act_vals=line.strip('\n').split()
            act_vals.remove("Actual")
            act_vals.remove("Values")
            continue
        if count_line==4:
           # print(count_line,line)
            pred_vals=line.strip('\n').split()
           # pdb.set_trace()
            pred_vals.remove("Predicted")
            pred_vals.remove("labels")
            word_c+=1
            if (len(act_vals)==len(pred_vals)):
                correct=0
                incorrect=0
                for i in zip(act_vals,pred_vals):
                    confusion[int(i[0])][int(i[1])]+=1
                    if i[0]==i[1]:
                        correct+=1
                    else:
                        incorrect+=1
                if correct/len(act_vals)*100>95:
                    type1.append((file_c,word_c))
                elif correct/len(act_vals)*100>85:#change if executed in all cases
                    type2.append((file_c,word_c))
                elif correct/len(act_vals)*100>75:
                    type3.append((file_c,word_c))
                elif correct/len(act_vals)*100<=75:
                    bad_words.append((file_c,word_c))
            else :
                    bad_words.append((file_c,word_c))
        count_line=0
        main_c+=1
        #pdb.set_trace()
            

        #pdb.set_trace()

with open("Word_List.txt","w") as f:
    print ("Type 1",len(type1),file=f)
    print ("Type 2",len(type2),file=f)
    print ("Type 3",len(type3),file=f)
    print ("Bad Words",len(bad_words),file=f)
    print ("Type 1",file=f)
    print (type1,file=f)
    print ("Type 2",file=f)
    print (type2,file=f)
    print ("Type 3",file=f)
    print (type3,file=f)
    print ("Bad Words",file=f)
    print (bad_words,file=f)


with open('conf_matrix.csv', 'w', newline='') as csvfile:
    spamwriter = csv.writer(csvfile, delimiter=' ')
    for row in confusion:
        spamwriter.writerow(row)
    
