import pickle
import pdb
from collections import Counter
import os
import xml.etree.ElementTree as ET
import re

def levenshtein(seq1, seq2):
    oneago = None
    thisrow = list(range(1, len(seq2) + 1)) + [0]
    for x in range(len(seq1)):
        twoago, oneago, thisrow = oneago, thisrow, [0] * len(seq2) + [x + 1]
        for y in range(len(seq2)):
            delcost = oneago[y] + 1
            addcost = thisrow[y - 1] + 1
            subcost = oneago[y - 1] + (seq1[x] != seq2[y])
            thisrow[y] = min(delcost, addcost, subcost)
    return thisrow[len(seq2) - 1]

def remove_values_from_list(the_list, val):
   return [value for value in the_list if value != val]

def changeXY(word):
    strokes=word.findall('stroke');
    for stroke in strokes:
        trace=stroke.findall('hwTrace/trace');
        points=trace[0].text.strip('\n').split()
        c=0
        while c<len(points)-1:
            points[c],points[c+1]=points[c+1],points[c];
            c+=2
        newString=" ".join(points)
        trace[0].text=newString
    return(word)
        #pdb.set_trace();

wordDb=pickle.load(open('word_db.p','rb'))
#pdb.set_trace()
predWordDb=[]
with open("log_uni_words.txt",'r')as f:
    for line in f:
        line=line.strip('\n');
        if ".xml" in line:
            #pdb.set_trace();
           # print (line)
            predWordDb.append([])
        elif "WORD" in line:
            continue
        else:
            chars=line.strip('\n').split()
            uniChars=[chr(int(k,16)) for k in chars]
            thisWord=''
            for char in uniChars:
                thisWord+=char
            predWordDb[-1].append(thisWord)
            #pred_word_db[-1].append()
        #pdb.set_trace()
#pdb.set_trace()

                
for path,dirs,fileList in os.walk(re.escape("links")): #checks for files in the dataset dir. runs only once
	fileList=sorted(fileList)
#pdb.set_trace()
totalWords=0
totalCharacters=0
totalDistance=0
wordsA=0
wordsB=0
wordsC=0
wordsD=0
#words80=0
#words70=0
#bad_words=0
confusion=[]
with open("wordA.txt",'w',encoding="utf-16") as f1:
    with open("wordB.txt",'w',encoding="utf-16") as f2:
        with open("wordC.txt",'w',encoding="utf-16") as f3:
            with open("wordD.txt",'w',encoding="utf-16") as f4:
                for c1,i in enumerate(zip(wordDb,predWordDb)):
                    correctWords=0;
                    writeRoot = ET.Element("x")
                    listOfLinks=pickle.load(open("links/"+fileList[c1],'rb'))
                    writeRoot.append(listOfLinks[0]);writeRoot.append(listOfLinks[1]);writeRoot.append(listOfLinks[2]);writeRoot.append(ET.Element('annotationDef'));
                    document=ET.SubElement(writeRoot[-1],'document');
                    originCode=ET.SubElement(document,'originCode');originCode.text=" 0 ";
                    noOfPages=ET.SubElement(document,'noOfPages');
                    #if len(i[0])>120:
                     #   i=(i[0][len(i[0])-120::],i[1][len(i[1])-120::])
                     #  list_of_links=list_of_links[len(list_of_links)-120::]
                    #pdb.set_trace();    
                    print (fileList[c1].replace("_link_db.p",""),file=f1)
                    print (fileList[c1].replace("_link_db.p",""),file=f2)
                    print (fileList[c1].replace("_link_db.p",""),file=f3)
                    print (fileList[c1].replace("_link_db.p",""),file=f4)
                    for c2,j in enumerate(zip(i[0],i[1])):
                        if ('\ufeff' in j[0]) or ('\ufeff' in j[1]):
                            j=(list(filter(('\ufeff').__ne__, j[0])),list(filter(('\ufeff').__ne__, j[1])))
                            x='';y='';
                            for i in j[0]:
                                x+=i;
                            for i in j[1]:
                                y+=i;
                            j=(x,y);
                        totalWords+=1
                        totalCharacters+=len(j[0]);
                        dis=levenshtein(j[0],j[1]);
                        totalDistance+=dis;
                        if dis==0:
                            correctWords+=1;
                            wordsA+=1;
                            page=ET.SubElement(document,'page');
                            pageNo=ET.SubElement(page,'pageNo');pageNo.text=str(correctWords);
                            pageId=ET.SubElement(page,'pageId');pageId.text="Dataset(IIIT-H)";
                            lineCount=ET.SubElement(page,'lineCount');lineCount.text=" 1 ";
                            line=ET.SubElement(page,'line');
                            lineNumber=ET.SubElement(line,'lineNumber');lineNumber.text=" 1 ";
                            truthLevel=ET.SubElement(line,'truthLevel');truthLevel.text="truthed";
                            wordCount=ET.SubElement(line,'wordCount');wordCount.text=" 1 ";
                            word=changeXY(listOfLinks[c2+3]);
                            word[0].text=" 1 "
                            line.append(word)
                            print (j[0],j[1],dis,sep='\t',file=f1)
                            
                            #writeRoot.append(listOfLinks[c2+3])
                        elif dis==1:
                            wordsB+=1;
                            print (j[0],j[1],dis,sep='\t',file=f2)
                        elif dis==2:
                            wordsC+=1
                            print (j[0],j[1],dis,sep='\t',file=f3)
                        else:
                            wordsD+=1
                            print (j[0],j[1],dis,sep='\t',file=f4)
                        #pdb.set_trace()
                    #pdb.set_trace();
                    #ET.dump(writeRoot);
                    noOfPages.text=str(correctWords);
                    writeTree=ET.ElementTree(writeRoot)
                    writeTree.write("typeOneXML/"+fileList[c1].replace("_link_db.p",""))
with open("statistics.txt",'w',encoding="utf-16") as f5:
    print("Total - ",totalWords,file=f5)
    print("Class A - ",wordsA,file=f5)
    print("Class B - ",wordsB,file=f5)
    print("Class C - ",wordsC,file=f5)
    print("Class D - ",wordsD,file=f5)
    print("Total Characters - ",totalCharacters,file=f5)
    print("Edit Distance Accuracy - ",100-(totalDistance/totalCharacters*100),file=f5)
    



                    
