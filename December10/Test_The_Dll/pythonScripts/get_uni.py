from __future__ import print_function
import os
import pdb
import xml.etree.ElementTree as ET
import pickle


def parse(data_directory,n_files):

	'''class_labels=['cl' for i in range(n_class)]#class label array of dimension n_class#
	word_database_1=[[] for k in range(n_class)]##This will store the samples of 120 words with the traces combined. This will be a 120*30*n list ##'''


	for path,dirs,filelist in os.walk(data_directory): #checks for files in the dataset dir. runs only once
		filelist=sorted(filelist)
	#pdb.set_trace()
	word_db=[]
	link_db=[]
	for c1,filename in enumerate(filelist):
		#if c1<n_files:
		#	continue
		c_c=0
		current_file=data_directory+'/'+filename
		print (current_file)
		tree = ET.parse(current_file)
		root = tree.getroot()
		words_per_file=[]
		links_per_file=[]
	
		pages=root.findall('annotationDef/document/page')
		
		#print ('Processing File '+current_file)
		for i in pages:
			##next line lists the lines in each page##
			lines=i.findall('line')
			for this_line in lines:
				#print 'Processing page no. '+this_page.text
				words=this_line.findall('word')
				#print lines_this_page
				for this_word in words:
					label=this_word.findall('labelDesc/annotationDetails/codeSequence')
					stroke_c=int(this_word.findall('strkCount')[0].text)
					if stroke_c>3:
						words_per_file.append(label[0].text.strip().strip('\n').strip('\ufeff'))
						links_per_file.append(this_word)
					#pdb.set_trace()
		word_db.append(words_per_file)
		#link_db.append(links_per_file)
		pickle.dump(links_per_file,open("links/"+filename+'_link_db.p','wb'))
	
	#pdb.set_trace()			
	pickle.dump(word_db,open('word_db.p','wb'))
	


	       
data_directory="NewXML"
n_files=387
parse(data_directory,n_files)
