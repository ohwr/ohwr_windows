import pdb
import os
import xml.etree.ElementTree as ET
writeRoot = ET.Element("document")
data_directory="NewXML";
class WordXml:
    def __init__(self,word_no,no_strokes,uniString):
        self.head=ET.Element("word")
        ET.SubElement(self.head,"wordNumber")
        ET.SubElement(self.head,"uniString")
        ET.SubElement(self.head,"numberOfStrokes")
        ET.SubElement(self.head,"strokes")

#Start parsing the dataset
for path,dirs,filelist in os.walk(data_directory): #checks for files in the dataset dir. runs only once
		filelist=sorted(filelist)

for c1,filename in enumerate(filelist):
        current_file=data_directory+'/'+filename
        tree = ET.parse(current_file)
        root = tree.getroot()
        pages=root.findall('annotationDef/document/page')
        for i in pages:
            ##next line lists the lines in each page##
            lines=i.findall('line')
            for this_line in lines:
                    #print 'Processing page no. '+this_page.text
                    words=this_line.findall('word')
                    #print lines_this_page
                    for this_word in words:
                            label=this_word.findall('labelDesc/annotationDetails/codeSequence')
                            writeRoot.append(this_word)
                            writeTree=ET.ElementTree(writeRoot)
                            writeTree.write("typeOneDataset.xml")
                            pdb.set_trace();
                            #words_per_file.append(label[0].text.strip().strip('\n').strip('\ufeff'))
'''newWord=WordXml(0,10,"OHWR")
writeRoot.append(newWord.head)
writeTree=ET.ElementTree(writeRoot)
writeTree.write("typeOneDataset.xml")
#print (root.tag)'''
