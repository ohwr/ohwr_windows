import pickle
import pdb
from collections import Counter
import os
import xml.etree.ElementTree as ET
import re


word_db=pickle.load(open('word_db.p','rb'))
#pdb.set_trace()
pred_word_db=[]
with open("log_uni_words.txt",'r')as f:
    for line in f:
        line=line.strip('\n');
        if ".xml" in line:
            #pdb.set_trace();
           # print (line)
            pred_word_db.append([])
        elif "WORD" in line:
            continue
        else:
            chars=line.strip('\n').split()
            uni_chars=[chr(int(k,16)) for k in chars]
            this_word=''
            for char in uni_chars:
                this_word+=char
            pred_word_db[-1].append(this_word)
            #pred_word_db[-1].append()
        #pdb.set_trace()
pdb.set_trace()

                
for path,dirs,filelist in os.walk(re.escape("links")): #checks for files in the dataset dir. runs only once
	filelist=sorted(filelist)
#pdb.set_trace()
tot_words=0
wrong_words=0
words100=0
words95=0
words90=0
words85=0
words80=0
words70=0
bad_words=0
confusion=[]
with open("correct_results.txt",'a',encoding="utf-16") as f1:
    with open("worst_results.txt",'a',encoding="utf-16") as f2:
        with open("incorrect_results.txt",'a',encoding="utf-16") as f3:
            for c1,i in enumerate(zip(word_db,pred_word_db)):
                writeRoot = ET.Element("document")
                list_of_links=pickle.load(open("links/"+filelist[c1],'rb'))
                
                if len(i[0])>120:
                    i=(i[0][len(i[0])-120::],i[1][len(i[1])-120::])
                    list_of_links=list_of_links[len(list_of_links)-120::]
                #pdb.set_trace();    
                print ("U0"+str(c1+1)+".xml",file=f1)
                print ("U0"+str(c1+1)+".xml",file=f2)
                print ("U0"+str(c1+1)+".xml",file=f3)
                for c2,j in enumerate(zip(i[0],i[1])):
                    if ('\ufeff' in j[0]) or ('\ufeff' in j[1]):
                        j=(list(filter(('\ufeff').__ne__, j[0])),list(filter(('\ufeff').__ne__, j[1])))
                        x='';y='';
                        for i in j[0]:
                            x+=i;
                        for i in j[1]:
                            y+=i;
                        j=(x,y);
                    tot_words+=1
                    if len(j[0])!=len(j[1]):
                        print(j[0],end='\t',file=f2)
                        print(j[1],len(j[0])-len(j[1]),sep='\t',file=f2)
                        wrong_words+=1
                    elif j[0]==j[1]:
                        print ("WORD",c2,sep=' ',end=':  ',file=f1)
                        print(j[0],end='\n',file=f1)
                        writeRoot.append(list_of_links[c2])
                        
                        #print("Correct",file=f1)
                        words100+=1
                    else:
                        print(j[0],j[1],sep='\t',end='\t',file=f3)
                        match=0.0
                        
                        for k in zip(j[0],j[1]):
                            if k[0]==k[1]:
                                match+=1
                            else:
                                print(k[0],"-->",k[1],sep=' ',end='|',file=f3)
                                confusion.append(k[0]+"-->"+k[1])
                        mp=match/len(j[0])*100
                        print (mp,file=f3)
                        if mp>=95.0:
                            words95+=1
                        elif mp>=90.0:
                            words90+=1
                        elif mp>=85.0:
                            words85+=1
                        elif mp>=80.0:
                            words80+=1
                        elif mp>=70.0:
                            words70+=1
                        else:
                            bad_words+=1
                writeTree=ET.ElementTree(writeRoot)
                writeTree.write("typeOneXML/"+filelist[c1]+"TypeOne.xml")
            print("Total No. of words ",tot_words,file=f1)
            print("100 %",words100,file=f1)
            print("95-100 %",words95,file=f1)
            print("90-95 %",words90,file=f1)
            print("85-90 %",words85,file=f1)
            print("80-85 %",words80,file=f1)
            print("70-80 %",words70,file=f1)
            print("Below 70 %",bad_words,file=f1)
            print("Incorrect Length",wrong_words,file=f1)
            print(Counter(confusion),file=f3)




                
