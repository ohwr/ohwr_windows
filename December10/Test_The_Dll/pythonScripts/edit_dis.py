import pickle
import pdb
from collections import Counter
import os


def levenshtein(seq1, seq2):
    oneago = None
    thisrow = list(range(1, len(seq2) + 1)) + [0]
    for x in range(len(seq1)):
        twoago, oneago, thisrow = oneago, thisrow, [0] * len(seq2) + [x + 1]
        for y in range(len(seq2)):
            delcost = oneago[y] + 1
            addcost = thisrow[y - 1] + 1
            subcost = oneago[y - 1] + (seq1[x] != seq2[y])
            thisrow[y] = min(delcost, addcost, subcost)
    return thisrow[len(seq2) - 1]

def remove_values_from_list(the_list, val):
   return [value for value in the_list if value != val]



word_db=pickle.load(open("word_db.p","rb"));
#pdb.set_trace()
pred_word_db=[]
tot_chars=0
dis=0


with open('log_uni_words.txt','r')as f:
    for line in f:
        line=line.strip('\n');
        if ".xml" in line:
           # print (line)
            pred_word_db.append([])
        elif "WORD" in line:
            continue
        else:
            chars=line.strip('\n').split()
            uni_chars=[chr(int(k,16)) for k in chars]
            this_word=''
            for char in uni_chars:
                this_word+=char
            pred_word_db[-1].append(this_word)
           
for c1,i in enumerate(zip(word_db,pred_word_db)):
    print (c1);
    if len(i[0])!=len(i[1]):
        print ("Error!");
    #if len(i[0])>120:
     #   i=(i[0][len(i[0])-120::],i[1][len(i[1])-120::])
    
    
    
    for c2,j in enumerate(zip(i[0],i[1])):
        #pdb.set_trace();
        
        #pdb.set_trace()
        if (len(j[1])>1 and len(j[0])>1):
            
            if ('\ufeff' in j[0]) or ('\ufeff' in j[1]):
                j=(list(filter(('\ufeff').__ne__, j[0])),list(filter(('\ufeff').__ne__, j[1])))
                x='';y='';
                for i in j[0]:
                    x+=i;
                for i in j[1]:
                    y+=i;
                j=(x,y);
            #if len(j[0])==len(j[1]):
            tot_chars=tot_chars+len(j[0])
                    #list(filter(('\ufeff').__ne__, j[0]))
                    #j[0]=remove_values_from_list(j[0],'\ufeff');
                    #pdb.set_trace()
                # if ('\ufeff' in j[1]):
                    #list(filter(('\ufeff').__ne__, j[1]))
                    #j[1]=remove_values_from_list(j[1],'\ufeff');
                #    pdb.set_trace();
                    #print (j)
            dis+=levenshtein(j[0],j[1])
                #pdb.set_trace();
               
print("Accuracy is ",100 - (dis/tot_chars)*100)
pdb.set_trace()




                
