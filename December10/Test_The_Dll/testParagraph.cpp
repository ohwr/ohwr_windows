#include<string>
#include <fstream>
//#include <tchar.h>
#include<iostream>
#include <sstream>
//#include<conio.h>
#include"dirent.h"

#include "rapidxml\\rapidxml.hpp"
#include "rapidxml\\rapidxml_print.hpp"
#include "rapidxml\\rapidxml_utils.hpp"
#include "rapidxml\\rapidxml_iterators.hpp"

#include "..\\RecognitionEngine v 2.0\\include\\RecognitionEngine.h"
//#include "File_Data_Struct.h"

#include<conio.h>
using namespace std;
using namespace rapidxml;
//typedef int (__cdecl *MYPROC)(LPWSTR);



//init hook_uninstall_func;/// PT



typedef struct{
	vector<int> stroke_labels;
	int stroke_count;
	//wchar_t* uni_val;
	string uni_val;
}PARAGRAPH_DETAIL;

typedef struct{
	vector<PARAGRAPH_DETAIL> annotation_detail;
	vector<FILE_STROKEINFO*> paragraph_db;

}PARAGRAPH_DATABASE;


typedef int (*f_funci)(const char*,int,DEVICE_INFO_T);
typedef int (*f_funci1)(const FILE_STROKEINFO* , int , int , TEXT_BLOCK_UNI_INFO* );


PARAGRAPH_DATABASE parse_data(string);
vector<std::string> split(string);


int main (void){
	/*const char * ver = "Hello";
	fn_EngineVersionInfo(&ver);
	cout<< ver <<endl;*/
	//FILE_STROKEINFO a=parse_data();
	vector<string> dir_list;dir_list.clear();
	//WORD_DATABASE wd=parse_data("S001.xml");




	DIR *dir;
	struct dirent *ent;
	if ((dir = opendir ("ParagraphData")) != NULL) {
		/* print all the files and directories within directory */
		while ((ent = readdir (dir)) != NULL) {
			if (string(ent->d_name).compare(".") == 0 || string(ent->d_name).compare("..") == 0)
				continue;

			else
			{
				printf ("%s\n", ent->d_name);
				dir_list.push_back(ent->d_name);
			}
		}
		closedir (dir);
	} else {
		/* could not open directory */
		perror ("");
		return EXIT_FAILURE;
	}


	const char* WorkingDirPath="";


	HINSTANCE hinstLib; 
	//    MYPROC ProcAdd; 
	BOOL fFreeResult, fRunTimeLinkSuccess = FALSE; 
	// Get a handle to the DLL module.

	hinstLib = LoadLibrary(TEXT("RecognitionEngine.dll"));

	int UserID=1;
	if (hinstLib != NULL)
	{
		f_funci init = (f_funci)GetProcAddress(hinstLib,"fn_InitEngine");
		f_funci1 recoTB = (f_funci1)GetProcAddress(hinstLib,"fn_RecognizeTextBlock");
		int a = init(WorkingDirPath,1,TOP_LEFT);
		cout<< "Init Flag : " << a << endl;
		string logname103 ("log_uni_words.txt");
		ofstream log_uni_words(logname103.c_str(),ios::app);
		

		for (int count_dir=0;count_dir<dir_list.size();count_dir++)
			//for (int count_dir=2;count_dir<10;count_dir++)
		{
			//if (count_dir!=33)
			//continue;
			PARAGRAPH_DATABASE pd;
			cout<<"Parsing "<< dir_list[count_dir] <<endl;
			pd=parse_data(dir_list[count_dir]);
			//cout<<"Paragraph Database Size : "<< pd.paragraph_db.size()<<endl;
			//log_cl<<"*****************"<<dir_list[count_dir]<<"******************"<<endl;
			log_uni_words<<dir_list[count_dir]<<endl;
			for (int c3=0;c3 < pd.paragraph_db.size();c3++)
				//for (int c3=0;c3<10;c3++)
			{
				//if (wd.annotation_detail[c3].stroke_count>1)
				//{


				std::remove(pd.annotation_detail[c3].uni_val.begin(),pd.annotation_detail[c3].uni_val.end(),' ');

				log_uni_words<<"WORD "<<c3<<endl;
				int b=0;

				TEXT_BLOCK_UNI_INFO tbui;


				/*TCHAR** strOut;
				strOut = new TCHAR *[50];
				for(int ii = 0; ii <10; ii++)
				{
				strOut[ii] = new TCHAR[50];
				}
				cout<<"Word "<<c3<<" "<<endl;
				*/


				cout<<endl<<"Paragraph Number : "<<c3<<endl;
				b = recoTB(pd.paragraph_db[c3],pd.annotation_detail[c3].stroke_count,1, &tbui);
				cout << "TestParagrapH open" << endl;
				for (int ii = 0; ii < tbui.iNoOfLines; ii++)
				{
					for (int ij = 0; ij < tbui.ptrLineSegInfo[ii].iNoOfWords; ij++)
					{
						for (int ik = 0; ik < tbui.ptrLineSegInfo[ii].ptrWordSegInfo[ij].iNoOfStrokes; ik++)
						{
							for (int il = 0; il < 10; il++ )
							{
								if (tbui.ptrLineSegInfo[ii].ptrWordSegInfo[ij].strUniOut[0][il] == '\0')
									continue;
								printf("0%X ",tbui.ptrLineSegInfo[ii].ptrWordSegInfo[ij].strUniOut[0][il] );
							}
							cout << "   ";
						}
						cout << endl;
					}
				}
				cout << "Test paragraph close" << endl;
				


			}

		}
		log_uni_words.close();
		fFreeResult = FreeLibrary(hinstLib); 


	}


	return 0;

}

PARAGRAPH_DATABASE parse_data(string filename)
{
	filename="ParagraphData/"+filename;
	cout<<filename<<endl;
	PARAGRAPH_DATABASE pd;
	int c11=0;
	rapidxml::file<> xmlFile(&filename[0]);//http://stackoverflow.com/questions/2808022/how-to-parse-the-xml-file-in-rapidxml
	rapidxml::xml_document<> doc;
	doc.parse<0>(xmlFile.data());
	//vector<vector<FILE_STROKEINFO>> word_db;

	//xml_node<>* cur_page = doc.first_node()->first_node("annotationDef")->first_node("document")->first_node("page");
	//http://www.ffuts.org/blog/quick-notes-on-how-to-use-rapidxml/
	for (xml_node<> *curr_page = doc.first_node()->first_node("annotationDef")->first_node("document")->first_node("page");
		curr_page; curr_page = curr_page->next_sibling())
	{
		for(xml_node<>* curr_paragraph=curr_page->first_node("paragraph");curr_paragraph;curr_paragraph=curr_paragraph->next_sibling())
		{

			//for(xml_node<>* curr_word=curr_line->first_node("word");curr_word;curr_word=curr_word->next_sibling())
			//{
			//string temp_stk_ct=(curr_paragraph->first_node("akshara")->first_node("strokeGroup")->first_node("strokeCount")->value());
			//string temp_stk_ct=(curr_paragraph->);
			//cout << "Name @ paragraph node : " << temp_stk_ct <<endl;
			xml_attribute<> *attr = curr_paragraph->first_attribute();
			string paraNum = attr->value();
			attr = attr->next_attribute();
			string temp_stk_ct = attr->value();
			double stk_ct=::atof(temp_stk_ct.c_str());
			//cout << "Paragraph Number : "<< paraNum << "  Stroke Count : "<< stk_ct <<endl;

			FILE_STROKEINFO* my_paragraph ;
			my_paragraph=new FILE_STROKEINFO[int(stk_ct)];
			PARAGRAPH_DETAIL this_detail;


			this_detail.stroke_count=stk_ct;
			//this_detail.uni_val=(wchar_t*)(curr_word->first_node("labelDesc")->first_node("annotationDetails")->first_node("codeSequence")->value());
			this_detail.uni_val=curr_paragraph->first_node("labelDesc")->value();
			int c10=0;
			for(xml_node<>* curr_stroke=curr_paragraph->first_node("stroke");curr_stroke;curr_stroke=curr_stroke->next_sibling())
			{


				string raw_points = curr_stroke->first_node("hwTrace")->value();
				//cout<<raw_points<<endl;

				vector<std::string> sep_points=split(raw_points);
				FILE_POINTINFO* my_stroke;
				my_stroke=new FILE_POINTINFO[int(sep_points.size())/2];
				int c1=0;
				//cout<<sep_points.size()<<endl;
				int cur_tr=0;
				while (c1<sep_points.size())
				{
					//cout<<c1<<endl;
					FILE_POINTINFO this_point2;

					double this_y=::atof(sep_points[c1+1].c_str());
					double this_x=::atof(sep_points[c1].c_str());


					this_point2.x=this_x;
					this_point2.y=this_y;
					//if (cur_tr==0)
					//cout<<this_x<<" "<<this_y<<" ";
					//cout<<this_point2.x<<" "<<this_point2.y<<" ";
					//cout<<this_point2.x<<" "<<this_point2.y<<endl;
					this_point2.p=1;
					my_stroke[c1/2]=this_point2;
					c1=c1+2;
					cur_tr=1;

				}


				FILE_STROKEINFO my_stroke1;
				my_stroke1.ptrPointInfo=my_stroke;
				my_stroke1.strColor=1;
				my_stroke1.nPointCount=sep_points.size()/2;
				my_paragraph[c10]=my_stroke1;
				//delete [] my_stroke;
				c10++;



			}
			pd.annotation_detail.push_back(this_detail);
			pd.paragraph_db.push_back(my_paragraph);
			c11++;
			//delete [] my_word;
			//}

		}

	}

	return(pd);
}
vector<std::string> split(string input) {
	stringstream buffer(input);
	std::vector<std::string> ret;

	std::copy(istream_iterator<std::string>(buffer),
		istream_iterator<std::string>(),
		back_inserter(ret));
	return ret;
}
