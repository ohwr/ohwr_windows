#ifndef _CLASSIFIER_H
#define _CLASSIFIER_H

#include"..\stroke\stroke.h"
#include "svm.h"
#include<math.h>
class Classifier
{
	private:
		// output vector
		vector <int> label;
		vector <double> probability;		
	
		//input vector
		vector < vector <double> > local;
		vector <double> global;
	
		//vector<vector<double> >global_conf_prob;	
		//for finding the output of neural networks...
		vector<double> temp_global;	
		int Global();
		int Intermediate();
		int Discriminating();
	
 		int readFromResFile(ifstream &);
	    int writeToHtkFile();
 		//int initialize_class_list();	
		vector<int>local_class_list;
	
	 
		
	

	public:
		//int initialize_scale_list();
		//int predict_svm_scale(vector<double> &feature);

	
		Classifier();
		~Classifier();
		int Classify(Stroke &STK,svm_model* model1,vector<vector<int> > &iiit_global_conf_class,vector<vector<double> > &iiit_global_conf_prob, int clType, vector<vector<int> >&iiit_ClassType);
};
#endif
