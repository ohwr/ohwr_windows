/* X1mlp.param */
/* norm: LDA with SIMPLE */
/* 40 features */

/* cost_func: SQ_ERR */
/* sigm: SIGM */
/* net: 40,45,254 */
#include "Classifier.h"
#include "svm.h"
#include <stdio.h>
#include <math.h>
#include <vector>
#include <algorithm>
#include <string>
//string iiit_diction_path (".\\Engines\\IIITH\\MAL\\");
 /* function declarations */
#define Malloc(type,n) (type *)malloc((n)*sizeof(type))



void swapFloat(float *flt)
{
        char *temp;
        char tmp;

        temp = (char *)flt;
        tmp = *temp;
        *temp = *(temp+3);
        *(temp+3) = tmp;
        tmp = *(temp+1);
        *(temp+1) = *(temp+2);
        *(temp+2) = tmp;

        return;
}

Classifier::Classifier()
{
	label.clear();
	probability.clear();
	
	for(int i=0;i<(int)local.size();i++)
	{
		local[i].clear();
	}
	local.clear();
	global.clear();
	local_class_list.clear();
	//initialize_class_list();
}

Classifier::~Classifier()
{
	
}


double predict_svm_new(vector<double> &feature,svm_model* model,int CType,vector<vector<int> >&iiit_ClassType)
{
	char* line;
	int max_line_len = 1024;
	struct svm_node *x;
	int max_nr_attr = 64;
	int predict_probability=0;
	//cout<<"enter here"<<endl;
	//cout<<"feature_size: "<<feature.size()<<endl;
	line = (char *) malloc(max_line_len*sizeof(char));
	x = (struct svm_node *) malloc(max_nr_attr*sizeof(struct svm_node));

	int correct = 0;
	int total = 0;
	double error = 0;
	double sumv = 0, sumy = 0, sumvv = 0, sumyy = 0, sumvy = 0;

	int svm_type=svm_get_svm_type(model);
	int nr_class=svm_get_nr_class(model);
	int *labels=(int *) malloc(nr_class*sizeof(int));
	svm_get_labels(model,labels);
	//double *prob_estimates=NULL;



	//		cout << countSample++ << endl; // Printing Sample Number -- 

	int i = 0;

	double v;


	while(1)
	{
		if(i>=max_nr_attr-1)	// need one more for index = -1
		{
			max_nr_attr *= 2;
			x = (struct svm_node *) realloc(x,max_nr_attr*sizeof(struct svm_node));
		}

		x[i].index=i+1;
		x[i].value=feature[i];
		++i;
		if(i>(int(feature.size())-1))
			break;
	}	
	x[i++].index = -1;
	//cout<<"i: "<<i<<"in predict "<<endl;
	/////PT June 19 edits
	v = svm_predict(model,x);
	CType=CType-1;
	double *prob_estimates =  Malloc(double,nr_class);
	vector<int> ClassCons;ClassCons.clear();
	for(int i=0;i<(int)iiit_ClassType[CType].size();i++)
		ClassCons.push_back(iiit_ClassType[CType].at(i));
	svm_predict_probability(model, x, prob_estimates);
	vector<int> indofClassCons;indofClassCons.clear();
	for(int i=0;i<nr_class;i++)
		for(int j=0;j<(int)ClassCons.size();j++)
			if (labels[i]== ClassCons[j])
				indofClassCons.push_back(i);
	vector<double>probunderCons;probunderCons.clear();
	for(int i=0;i<(int)indofClassCons.size();i++)
		probunderCons.push_back(prob_estimates[indofClassCons[i]]);
	int indexofBiggest = distance(begin(probunderCons), max_element(begin(probunderCons), end(probunderCons)));
	int ClassofBiggest = labels[indofClassCons[indexofBiggest]];
	



	//svm_destroy_model(model);
	free(line);
	free(x);
	/// Change this to Class of Biggest
	//return ClassofBiggest;
	return v;
}	
int Classifier::Classify(Stroke &Stk,svm_model* model1,vector<vector<int> > &global_conf_class,vector<vector<double> > &global_conf_prob, int CType, vector<vector<int> >&iiit_ClassType)
{
	label.clear();
	probability.clear();
	  for(int i=0;i<(int)local.size();i++)
        {
                local[i].clear();
        }

	global.clear();
	local.clear();
	global=Stk.feat.global;
	local=Stk.feat.local;

	//initial classifier...
 	int svm_label=int(predict_svm_new(global,model1,CType,iiit_ClassType));
	///APR 16 PT
	svm_label -=0; ////Because classlabels start at 1 and array indexing at 0

	//next label classifier...
	vector<int> secondary_label;
	vector<double> secondary_prob;
	secondary_label.clear();
	secondary_prob.clear();
	//////////////////SPT's Edits
	/*ifstream class_fp;
	string temp_diction(iiit_diction_path);
	temp_diction +="data\\subsclasslabel.txt";
	class_fp.open(temp_diction.c_str());
    //if(class_fp.is_open()){
    int temp_total_label;
	int tmp; vector<int>classLab;classLab.clear();
    class_fp>>temp_total_label;
    for(int i=0;i<temp_total_label;i++){
        class_fp>>tmp;
        classLab.push_back(tmp);
	}
	//class_fp.close();
	int ser = svm_label;
    std::vector<int>::iterator it;
    it = find (classLab.begin(), classLab.end(), ser);   
	
    //cout<<it<<" Location of search";
    int subsClassLabel = (int)distance(classLab.begin(),it);*/
	///I'm not using subsClassLabel anymore. Just retaining the code
	/*for(int i=0;i<(int)global_conf_class[subsClassLabel-1].size();i++)
	{
		secondary_label.push_back(global_conf_class[subsClassLabel-1][i]);
		secondary_prob.push_back(global_conf_prob[subsClassLabel-1][i]);
	}*/
	for(int i=0;i<(int)global_conf_class[svm_label-1].size();i++)
	{
		secondary_label.push_back(global_conf_class[svm_label-1][i]);
		secondary_prob.push_back(global_conf_prob[svm_label-1][i]);
	}
	probability.clear();
	for(unsigned int i=0;i<secondary_label.size();i++)
	{	//cout<<svm_label<<" "<<secondary_label[i]<<"  "<<secondary_prob[i]<<"   "<<endl;
		label.push_back(secondary_label[i]);
		probability.push_back(secondary_prob[i]);
	}
	
	Stk.label.clear();
	Stk.probability.clear();
	//Stk.label.push_back(svm_label);
	Stk.label = label;

	Stk.probability= probability;
	//Stk.probability.push_back(global_conf_prob[subsClassLabel-1][1]);
	//cout<<"svm_label: "<<svm_label;	
	cout<<endl<<endl;
	return(1);
	}



int Classifier::Global()
{
	return(1);
}

// This is HMM stroke classifier 
int Classifier::readFromResFile(ifstream &in)
{
	char str[100];
        in.getline(str,100);
        in.getline(str,100);

	for(int i=0;i<5;i++)
	{	
		in>>str;
		in>>str;
		in>>str;

	        int p=0;
       		int j=1;
	        while(str[j]!='\0')
        	{
			p=p*10+(str[j]-'0');
			j++;	         
        	}
	
		double prob;
		in>>prob;
		label.push_back(p);
		probability.push_back(prob);
		in.getline(str,100);
		in.getline(str,100);
	}
        
	return(1);	
}

int Classifier::writeToHtkFile()
{

	ofstream out("testfile");
	int y=int(local.size());

	char buff[4],buff1[4];
	buff[3] = y & 0xff; y >>= 8;
	buff[2] = y & 0xff; y >>= 8;
	buff[1] = y & 0xff; y >>= 8;
	buff[0] = y & 0xff;
	out.write((char*)buff,4);//number of samples

	y=100000;
	buff[3] = y & 0xff; y >>= 8;
	buff[2] = y & 0xff; y >>= 8;
	buff[1] = y & 0xff; y >>= 8;
	buff[0] = y & 0xff;
	out.write((char*)buff,4);//period of samples

	
	y=12;
	buff[1] = y & 0xff; y >>= 8;
	buff[0] = y & 0xff;
	out.write((char*)buff,2);//total bytes in one samples


	y=0x0009;
	buff1[1] = y & 0xff; y >>= 8;
	buff1[0] = y & 0xff;
	out.write((char*)&buff1,2);//param

	for(int i=0;i<(int)local.size();i++)
	{
		float c=float(local[i][0]);
		float t=float(local[i][1]);
		float h=float(local[i][2]);

		swapFloat(&c);
		swapFloat(&t);
		swapFloat(&h);

		out.write((char*)&c,4);
		out.write((char*)&t,4);
		out.write((char*)&h,4);
	}
	out.close();
	return(1);
}


int Classifier::Intermediate()
{
	writeToHtkFile();
	ofstream hSyn ("../classifier/data/Hsyn");
        ofstream hDict("../classifier/data/hDict");
        ofstream hList("../classifier/data/hList");

        hSyn<< "(\n";
        hSyn<< "\tH"<<local_class_list[0]<<" ";
	 for(int i=0;i<(int)local_class_list.size();i++)
        {
                hDict <<"H"<<local_class_list[i]<<" "<<"H"<<local_class_list[i]<<endl;
                hList <<"H"<<local_class_list[i]<<endl;

                if(i)
                        hSyn<< "| H"<<local_class_list[i]<<" ";

        }
        hSyn<<"\n)";

        hSyn.close();
        hDict.close();
        hList.close();
	system("HParse ../classifier/data/Hsyn ../classifier/data/HNet");
	char cmd[1000];
	//finding best 5 out of 20
        sprintf(cmd,"HVite -w ../classifier/data/HNet -n 5 20 -d ../classifier/data/Models -i Res ../classifier/data/hDict ../classifier/data/hList testfile");
        system(cmd);



	ifstream in("Res");
	readFromResFile(in);
	in.close();
	system("rm ../classifier/data/HNet");
	system("rm testfile");	
	return(1);
}

int Classifier::Discriminating()
{
	return(1);
}
