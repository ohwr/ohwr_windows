#include"preProcess.h"
#include<algorithm>

preProcess::preProcess(void)
{
	X.clear();		
	Y.clear();		
	critPoints.clear();
}

preProcess::~preProcess(void)
{
	//nothing to be done for now		
}


// Loc is the location of origin that is used in the input device.
// After pre-process, the originals are left untouched;
// mid-proc and processed will be in the canonical reference frame
// used for the rest of the recognizer. processed will contain
// the pre-processed co-ordinates.
// Loc values are: (Copied from RecognitionEngine.h).
	typedef enum{
		TOP_LEFT=1,/**< enum value 0 */
		TOP_RIGHT, /**< enum value 1 */
		BTM_LEFT, /**< enum value 2 */
		BTM_RIGHT/**< enum value 3 */
	}OriginLocation;

int preProcess::Process(Stroke &Stk, int Loc)
{
	X.clear();
	Y.clear();
	critPoints.clear();	
	X = Stk.original.x;
	Y = Stk.original.y;
	double temp,temp1,maxX,maxY,minX,minY;
	int j1;

	// Compute maxY and maxX
	maxX = *max_element(X.begin(),X.end());
	maxY = *max_element(Y.begin(),Y.end());
	minX = *min_element(X.begin(),X.end());
	minY = *min_element(Y.begin(),Y.end());
	switch(Loc){
	case TOP_LEFT: //Input origin is TOP_LEFT	
		// Do a Y-flip (yi = maxY - yi) and shift in X to origin
		for(int i=0;i<(int)Y.size();i++)
		{
			Y[i] = maxY-Y[i];
			X[i] = X[i]-minX;
		}
		break;
	case TOP_RIGHT: //Input origin is TOP RIGHT
		// Do a X-flip (xi = maxX - xi)
		for(int i=0;i<(int)X.size();i++)
			X[i] = maxX-X[i];
		// Do a Y-flip (yi = maxY - yi)
		for(int i=0;i<(int)Y.size();i++)
			Y[i] = maxY-Y[i];
		break;
	case BTM_LEFT: //Input origin is BTM_LEFT
		// Shift min to the axes
		for(int i=0;i<(int)Y.size();i++)
		{
			Y[i] = Y[i] - minY;
			X[i] = X[i] - minX;
		}
		break;
	case BTM_RIGHT: //Input origin is BTM_RIGHT
		// Do a X-flip (xi = maxX - xi)
		for(int i=0;i<(int)X.size();i++)
		{
			X[i] = maxX - X[i];
			Y[i] = Y[i] - minY;
		}
		break;
	default:
		// Shift min to the axes
		for(int i=0;i<(int)Y.size();i++)
		{
			Y[i] = Y[i] - minY;
			X[i] = X[i] - minX;
		}
		break;
	}
	Stk.midproc.x= X;Stk.midproc.y=Y;
	normalization(100);
	equiDistant_Resample(3);
	smooth();
	equiDistant_Resample(5);
	normalization(100);
	Stk.processed.x = X;
	Stk.processed.y = Y;

	return 1;

	/*  // Beginning of Old Pre-process
	switch(Loc)
	{
	case 1: //TOP_LEFT	
		temp= *max_element(Y.begin(),Y.end());
		for(int i=0;i<(int)Y.size();i++)
			Y[i]=temp-Y[i];
		//// X-Y swap
		Stk.original.x.clear();Stk.original.y.clear();
		Stk.original.x=Y;Stk.original.y=X;
		X.clear();Y.clear();
		X = Stk.original.x;Y=Stk.original.y;
		////Both X and Y reversals New Addition 05th April 2013
		temp= *max_element(Y.begin(),Y.end());
		for(int i=0;i<(int)Y.size();i++)
			Y[i]=temp-Y[i];
		temp1= *max_element(X.begin(),X.end());
		for(int i=0;i<(int)X.size();i++)
			X[i]=temp1-X[i];
		Stk.original.x.clear();Stk.original.y.clear();
		Stk.original.x=X;Stk.original.y=Y;
		////Y reversal  16Apr 2013
		temp= *max_element(Y.begin(),Y.end());
		for(int i=0;i<(int)Y.size();i++)
			Y[i]=temp-Y[i];
		Stk.original.x.clear();Stk.original.y.clear();
		Stk.original.x=X;Stk.original.y=Y;
		Stk.midproc.x= X;Stk.midproc.y=Y;
		//reverse_char();
		normalization(100);
		equiDistant_Resample(3);
		smooth();
		equiDistant_Resample(5);
		normalization(100);
		Stk.processed.x = X;
		Stk.processed.y = Y;
		//X.clear();Y.clear();
		//X=Stk.midproc.x;Y=Stk.midproc.y;
		
		equiDistant_Resample(2);
		j1=(int)X.size()/40;
		for(int i=0;i<40;i++)
		{
			if(j1*i>(int)X.size())
			{
				Stk.equipoints.x.push_back(X[(int)X.size()-1]);
				Stk.equipoints.y.push_back(Y[(int)X.size()-1]);
			}
			else
			{
				Stk.equipoints.x.push_back(X[j1*i]);
				Stk.equipoints.y.push_back(Y[j1*i]);
			}
		}
		Stk.original.x.clear();Stk.original.y.clear();
		Stk.original.x=tempvecx;Stk.original.y=tempvecy;
		break;
	case 2: //TOP RIGHT--DIFF FROM TOP_LEFT IS X-REVERSAL. So just adding that. No mods in the middle.
		temp= *max_element(Y.begin(),Y.end());
		for(int i=0;i<(int)Y.size();i++)
			Y[i]=temp-Y[i];
		//// X-Y swap
		Stk.original.x.clear();Stk.original.y.clear();
		Stk.original.x=Y;Stk.original.y=X;
		X.clear();Y.clear();
		X = Stk.original.x;Y=Stk.original.y;
		////Both X and Y reversals New Addition 05th April 2013
		temp= *max_element(Y.begin(),Y.end());
		for(int i=0;i<(int)Y.size();i++)
			Y[i]=temp-Y[i];
		temp1= *max_element(X.begin(),X.end());
		for(int i=0;i<(int)X.size();i++)
			X[i]=temp1-X[i];
		Stk.original.x.clear();Stk.original.y.clear();
		Stk.original.x=X;Stk.original.y=Y;
		////Y reversal  16Apr 2013
		temp= *max_element(Y.begin(),Y.end());
		for(int i=0;i<(int)Y.size();i++)
			Y[i]=temp-Y[i];
		///X-reversal 18APR 2013
		temp= *max_element(X.begin(),X.end());
		for(int i=0;i<(int)X.size();i++)
			X[i]=temp-X[i];
		Stk.original.x.clear();Stk.original.y.clear();
		Stk.original.x=X;Stk.original.y=Y;
		Stk.midproc.x= X;Stk.midproc.y=Y;
		reverse_char();
		normalization(100);
		equiDistant_Resample(3);
		smooth();
		equiDistant_Resample(5);
		normalization(100);
		Stk.processed.x = X;
		Stk.processed.y = Y;
		//X.clear();Y.clear();
		//X=Stk.midproc.x;Y=Stk.midproc.y;
		
		equiDistant_Resample(2);
		j1=(int)X.size()/40;
		for(int i=0;i<40;i++)
		{
			if(j1*i>(int)X.size())
			{
				Stk.equipoints.x.push_back(X[(int)X.size()-1]);
				Stk.equipoints.y.push_back(Y[(int)X.size()-1]);
			}
			else
			{
				Stk.equipoints.x.push_back(X[j1*i]);
				Stk.equipoints.y.push_back(Y[j1*i]);
			}
		}
		Stk.original.x.clear();Stk.original.y.clear();
		Stk.original.x=tempvecx;Stk.original.y=tempvecy;
		break;
	case 3: //BTM_LEFT--DIFF FROM TOP_LEFT IS Y-REVERSAL. So just Removing that part. No mods in the middle.
		temp= *max_element(Y.begin(),Y.end());
		for(int i=0;i<(int)Y.size();i++)
			Y[i]=temp-Y[i];
		//// X-Y swap
		Stk.original.x.clear();Stk.original.y.clear();
		Stk.original.x=Y;Stk.original.y=X;
		X.clear();Y.clear();
		X = Stk.original.x;Y=Stk.original.y;
		////Both X and Y reversals New Addition 05th April 2013
		temp= *max_element(Y.begin(),Y.end());
		for(int i=0;i<(int)Y.size();i++)
			Y[i]=temp-Y[i];
		temp1= *max_element(X.begin(),X.end());
		for(int i=0;i<(int)X.size();i++)
			X[i]=temp1-X[i];
		Stk.original.x.clear();Stk.original.y.clear();
		Stk.original.x=X;Stk.original.y=Y;
		////MODDED HERE-- REMOVED 16 APR Y-reversal
		Stk.original.x.clear();Stk.original.y.clear();
		Stk.original.x=X;Stk.original.y=Y;
		Stk.midproc.x= X;Stk.midproc.y=Y;
		reverse_char();
		normalization(100);
		equiDistant_Resample(3);
		smooth();
		equiDistant_Resample(5);
		normalization(100);
		Stk.processed.x = X;
		Stk.processed.y = Y;
		//X.clear();Y.clear();
		//X=Stk.midproc.x;Y=Stk.midproc.y;
		
		equiDistant_Resample(2);
		j1=(int)X.size()/40;
		for(int i=0;i<40;i++)
		{
			if(j1*i>(int)X.size())
			{
				Stk.equipoints.x.push_back(X[(int)X.size()-1]);
				Stk.equipoints.y.push_back(Y[(int)X.size()-1]);
			}
			else
			{
				Stk.equipoints.x.push_back(X[j1*i]);
				Stk.equipoints.y.push_back(Y[j1*i]);
			}
		}
		Stk.original.x.clear();Stk.original.y.clear();
		Stk.original.x=tempvecx;Stk.original.y=tempvecy;
		break;
	case 4: //BTM_RIGHT--DIFF FROM BTM_LEFT IS X-REVERSAL. So just adding that part. No mods in the middle.
		temp= *max_element(Y.begin(),Y.end());
		for(int i=0;i<(int)Y.size();i++)
			Y[i]=temp-Y[i];
		//// X-Y swap
		Stk.original.x.clear();Stk.original.y.clear();
		Stk.original.x=Y;Stk.original.y=X;
		X.clear();Y.clear();
		X = Stk.original.x;Y=Stk.original.y;
		////Both X and Y reversals New Addition 05th April 2013
		temp= *max_element(Y.begin(),Y.end());
		for(int i=0;i<(int)Y.size();i++)
			Y[i]=temp-Y[i];
		temp1= *max_element(X.begin(),X.end());
		for(int i=0;i<(int)X.size();i++)
			X[i]=temp1-X[i];
		Stk.original.x.clear();Stk.original.y.clear();
		Stk.original.x=X;Stk.original.y=Y;
		////MODDED HERE-- Added X-reversal 18 APR
		///X-reversal 18APR 2013
		temp= *max_element(X.begin(),X.end());
		for(int i=0;i<(int)X.size();i++)
			X[i]=temp-X[i];
		Stk.original.x.clear();Stk.original.y.clear();
		Stk.original.x=X;Stk.original.y=Y;
		Stk.midproc.x= X;Stk.midproc.y=Y;
		reverse_char();
		normalization(100);
		equiDistant_Resample(3);
		smooth();
		equiDistant_Resample(5);
		normalization(100);
		Stk.processed.x = X;
		Stk.processed.y = Y;
		//X.clear();Y.clear();
		//X=Stk.midproc.x;Y=Stk.midproc.y;
		equiDistant_Resample(2);
		j1=(int)X.size()/40;
		for(int i=0;i<40;i++)
		{
			if(j1*i>(int)X.size())
			{
				Stk.equipoints.x.push_back(X[(int)X.size()-1]);
				Stk.equipoints.y.push_back(Y[(int)X.size()-1]);
			}
			else
			{
				Stk.equipoints.x.push_back(X[j1*i]);
				Stk.equipoints.y.push_back(Y[j1*i]);
			}
		}
		Stk.original.x.clear();Stk.original.y.clear();
		Stk.original.x=tempvecx;Stk.original.y=tempvecy;
		break;
	default:break;
	}
	*/  // End of Old Pre-process

	//process--- PT put all this in the each switch case
	/*reverse_char();
	normalization(100);
	equiDistant_Resample(3);
	smooth();
	equiDistant_Resample(5);
	normalization(100);
	
        Stk.processed.x = X;
        Stk.processed.y = Y;

	equiDistant_Resample(2);
	
	int j=(int)X.size()/40;
	for(int i=0;i<40;i++)
	{
		if(j*i>(int)X.size())
		{
			Stk.equipoints.x.push_back(X[(int)X.size()-1]);
			Stk.equipoints.y.push_back(Y[(int)X.size()-1]);
		}
		else
		{
			Stk.equipoints.x.push_back(X[j*i]);
			Stk.equipoints.y.push_back(Y[j*i]);
		}
	}*/
}
int preProcess::reverse_char()
{
	double maxY=*max_element(X.begin(),X.end());
	for(int i=0;i<(int)X.size();i++)
	{
		Y[i]+=maxY+2*(maxY-Y[i]);
	}
	return 1;
}
int preProcess::normalization(int normHeight)
{
   double ratio;
   double minX = *min_element(X.begin(),X.end());
   double maxX = *max_element(X.begin(), X.end());
   double minY = *min_element(Y.begin(), Y.end());
   double maxY = *max_element(Y.begin(), Y.end());
    if (maxY - minY != 0 && maxX-minX < (maxY-minY)*5)
        ratio = double(normHeight) / double(maxY - minY);
    else if (maxX - minX != 0)
        ratio = double(normHeight) / double(maxX - minX);
    else
        ratio = 1.0;
    double centerx,centery;
    centerx=(maxX+minX)/double(2);
    centery=(maxY+minY)/double(2);

    for (int i=0; i < int(X.size()); i++)
    {
        X[i] = (X[i]-centerx) * ratio + 200;
        Y[i] = (Y[i]-centery) * ratio + 200;
    }

/*    minX = int((minX-centerx) * ratio+200);
    maxX = int((maxX-centerx) * ratio+200);
    minY = int((minY-centery) * ratio+200);
    maxY = int((maxY-centery) * ratio+200);

    meanX = int(meanX * ratio);
    meanY = int(meanY * ratio);*/

    return 1;
}
int preProcess::smooth()
{
	gaussFilter(2);
	return 1;	
}

int preProcess::equiDistant_Resample(float pointSpacing)
{
    double prevdist, x1, y1, dx, dy, x_incr, y_incr, m=0, dist=0;
    vector<double> newx;
    vector<double> newy;
    vector<int> newCrit;

    newx.clear();
    newy.clear();
    newCrit.clear();	

    if(critPoints.size() == 0)
          computeCritPoints();

    // Preserve first point
    newx.push_back(X[0]);
    newy.push_back(Y[0]);
    newCrit.push_back(0);

    prevdist=0.0;

    for(int p=0; p < int(critPoints.size())-1; p++)
    {
        for(int i=critPoints[p]; i < critPoints[p+1]; i++)
        {
            dx = X[i+1] - X[i];
            dy = Y[i+1] - Y[i];
            if (dx == 0.0)
            {
                x_incr=0;
                y_incr= ((dy<0)?(-1):1) * prevdist;
            }
            else
            {
                m = dy / dx;
                x_incr = double(((dx<0)?(-1):1) * prevdist * sqrt(1.0 / (m*m + 1)));
                y_incr = m * x_incr;
            }
            x1 = X[i] - x_incr;
            y1 = Y[i] - y_incr;
            dist = double(sqrt((dx*dx) + (dy*dy)) + prevdist);

            if (dx == 0.0)
            {
                x_incr = 0;
                y_incr = ((dy<0)?(-1):1) * pointSpacing;
		 }
            else
            {
                x_incr = float(((dx<0)?(-1):1) * pointSpacing * sqrt(1.0 / (m*m + 1)));
                y_incr = m * x_incr;
            }
            for (int j=0; j < int(dist/pointSpacing); j++)
            {
                x1 += x_incr;
                y1 += y_incr;
                newx.push_back(x1);
                newy.push_back(y1);
            }
            prevdist = dist - ((int)(dist/pointSpacing))*pointSpacing;
        }
        
	// Preserve final point
        if (prevdist > 0)
        {
            newx.push_back(X[critPoints[p+1]]);
            newy.push_back(Y[critPoints[p+1]]);
            prevdist = 0.0;
        }
        // Update critical point
        
	newCrit.push_back(int(newx.size()-1));
    }

    X.swap(newx);
    Y.swap(newy);
    critPoints.swap(newCrit);
    
    return 1;
}

int preProcess::fitToSpline()
{
	return 1;
}


int preProcess::gaussFilter(int sigma)
{
    double total, val, tempX, tempY;
    int ioffset;
    int gFilterSize;
    int halfFilterSize;

    vector<double> newx;
    vector<double> newy;
    vector<double> gFilter;

    newx.clear();
    newy.clear();
    gFilter.clear();

    if(critPoints.size() == 0)
          computeCritPoints();

    halfFilterSize = 3*sigma;   // Value decays to 1% by 3*sigma
    gFilterSize = 6*sigma+1;

    // Generate the filter kernel
    total = 1.0;
    for(int i=0; i < halfFilterSize; i++)
    {
                val = double(exp(-0.5*((halfFilterSize-i)*(halfFilterSize-i))/(sigma*sigma)));
                gFilter.push_back(val);
                total += 2*val;
    }
    gFilter.push_back(1.0);

    for(int i=0; i <= halfFilterSize; i++)
                gFilter[i] /= total;

    for(int i=halfFilterSize-1; i >=0; i--)
                gFilter.push_back(gFilter[i]);


    // Gaussian low-pass filter
    for(int k=0; k<int(critPoints.size())-1; k++)
    {
                newx.push_back(X[critPoints[k]]);
                newy.push_back(Y[critPoints[k]]);
                for(int i=critPoints[k]+1; i < critPoints[k+1]; i++)
                {
                        tempX = tempY = 0.0;
                        for(int j=0; j < gFilterSize; j++)
                        {
                        ioffset = j - halfFilterSize;
                        if (i + ioffset < critPoints[k] || i + ioffset > critPoints[k+1])
                        {
                                tempX += gFilter[j] * X[i];
                                tempY += gFilter[j] * Y[i];
                        }
                        else
                        {
                                tempX += gFilter[j] * X[i + ioffset];
                                tempY += gFilter[j] * Y[i + ioffset];
                        }
                        }
                        newx.push_back(tempX);
                        newy.push_back(tempY);
                }
    }
    newx.push_back(X.back());
    newy.push_back(Y.back());

    X.swap(newx);
    Y.swap(newy);

    return true;
}

double preProcess::computeDistance(int i,int j)
{
    return(sqrt( ( (X[j]-X[i])*(X[j]-X[i])  + (Y[j]-Y[i]) * (Y[j]-Y[i]) ) ) );
}

int preProcess::computeCritPoints()
{
 	vector <double> vel;
	critPoints.clear();
        vel.clear();
        critPoints.push_back(0);

        vel.push_back(0);
        for(int i=1;i < int(X.size())-1;i++)
        {
                double v=computeDistance(i,i-1);
                vel.push_back(v);
        }

        vel.push_back(0);
        for (int i=4; i < int(vel.size())-4; i++)
        {
                double tmp1 = vel[i]-vel[i-1];
                double tmp2 = vel[i+1]-vel[i];

                if(tmp1*tmp2 < 0 && tmp1 < 0 )
                {
                        if(vel[i] < vel[i-2] && vel[i] < vel[i+2] && vel[i] < vel[i-3] && vel[i] < vel[i+3] )
                        {
 				critPoints.push_back(i);
                                i += 2;   
                        }
                }
        }
    	critPoints.push_back(int(X.size())-1);
        return(int(critPoints.size()));
}