#include"postProcess.h"

#include<algorithm>
//#include "lex.cpp"
string int_to_string(int i) {
	ostringstream buffer;
	buffer << i;
	return buffer.str();
}

PostProcess::PostProcess()
{

	RECURSION_LIMIT=4;
	for(int i=0;i<(int)Stk_coll.size();i++)
	{
		Stk_coll[i].ClassLabel.clear();
		Stk_coll[i].ClassProb.clear();
		Stk_coll[i].X.clear();
		Stk_coll[i].Y.clear();
	}
	Stk_coll.clear();
	max_size = 0;
	return;
}
bool cmp_prob(struct stroke_sort a, struct stroke_sort b)
{
	return (a.prob > b.prob);
}
PostProcess::~PostProcess()
{
	for(int i=0;i<(int)Stk_coll.size();i++)
	{
		Stk_coll[i].ClassLabel.clear();
		Stk_coll[i].ClassProb.clear();
		Stk_coll[i].X.clear();
		Stk_coll[i].Y.clear();
	}
	Stk_coll.clear();

}
void PostProcess::backtrack(unsigned int& curr_stroke, stk_state &curr_seg_state,vector<unsigned int> &Stk_coll_pos,stack<stroke_state> &curr_akshara, bool &tilde_flag, unsigned int end_s )
{         
	if(curr_stroke <end_s && Stk_coll_pos[curr_stroke]< Stk_coll[curr_stroke].ClassLabel.size()-1)
		Stk_coll_pos[curr_stroke]++;
	else
	{
		//cout<<"Reached here"<<endl;	
		do
		{
			Stk_coll_pos[curr_stroke]=0;
			curr_stroke--;
			stroke_state temp_state=curr_akshara.top();
			curr_akshara.pop();
			curr_seg_state=temp_state.state;
			Stk_coll_pos[curr_stroke]++;
			tilde_flag=false;
		}
		while(Stk_coll_pos[curr_stroke] > Stk_coll[curr_stroke].ClassLabel.size()-1 && !curr_akshara.empty());
		if(curr_akshara.empty() )
		{
			curr_seg_state=S;
			if(Stk_coll_pos[curr_stroke]==Stk_coll[curr_stroke].ClassLabel.size())
				curr_stroke++;
		}
	}
}
vector<string> PostProcess::get_unicode(string str)
{
	for(unsigned int i=0;i<Stk_type_lib.size();i++)
		if(Stk_type_lib[i].label.compare(str)==0)
			return Stk_type_lib[i].unicode;
	vector<string> some_str;
	some_str.clear();
	return some_str;
}

// Generate the unicode corresponding to curr_akshara into the buffer output. Return 1 on success, 0 on failure.
int PostProcess::unicode_generate(stack<stroke_state> curr_akshara, vector<string> &output)
{
	vector<string> UV,UC,UVM,UCM,UAnuVis;
	//cout<<"Came to unicode generation!!"<<endl;
	bool app_ck=false;
	vector<int> strokes;
	do
	{
		stroke_state temp_state=curr_akshara.top();
		strokes.insert(strokes.begin(),temp_state.stk_no);
		curr_akshara.pop();
	}while(!curr_akshara.empty());
	// Special case for two-stroke vowels
	if(strokes.size()==2)
	{
		string temp_label1=Stk_type_lib[strokes[0]].label;
		string temp_label2=Stk_type_lib[strokes[1]].label;
		//ofstream log_fp("CompStrokesTest",ios::app);
		////log_fp<<"Comp Stroke part"<<endl;
		////log_fp<<temp_label1<<"   "<<temp_label2<<endl;
		
		for(unsigned int i=0;i<Stk_comp_lib.size();i++)
		{
			if(Stk_comp_lib[i].type==SV && temp_label1.compare(Stk_comp_lib[i].sub1)==0 && temp_label2.compare(Stk_comp_lib[i].sub2)==0)
			{
				UV=Stk_comp_lib[i].unicode;	
				////log_fp<<"Unicode for Compstr: ";
				for(int jjjj =0;jjjj<UV.size();jjjj++)
					////log_fp<<UV[jjjj];
				output.clear();
				output.insert(output.end(),UV.begin(),UV.end());
				return 1;
			}
		}
		//log_fp.close();
	}
	string temp_vowel;
	string last_cons;
	for(unsigned int i=0;i<strokes.size();i++)
	{
		switch(Stk_type_lib[strokes[i]].type)
		{
		case SV:
			if(UC.size()!=0)
				return 0;
			if(UV.size()==0)
				UV=Stk_type_lib[strokes[i]].unicode;
			else
				return 0;
			break;
		case SC:
			// if(UV.size()!=0)
			//  	return 0;
			if(UC.size()!=0)
			{
				vector<string> tmp_ck=get_unicode("chandrakkala");
				bool test_flag=false;
				for(unsigned int j=0;j<Stk_comp_lib.size();j++)
					if(Stk_comp_lib[j].type==SC && Stk_comp_lib[j].sub1.compare(last_cons)==0 && Stk_comp_lib[j].sub2.compare(Stk_type_lib[strokes[i]].label)==0)
					{
						UC=Stk_comp_lib[j].unicode;
						test_flag=true;
						break;
					}
				if(!test_flag)
				{
					UC.insert(UC.end(),tmp_ck.begin(),tmp_ck.end());
					UC.insert(UC.end(),Stk_type_lib[strokes[i]].unicode.begin(),Stk_type_lib[strokes[i]].unicode.end());
				}
			}
			else
			{
				UC.insert(UC.end(),Stk_type_lib[strokes[i]].unicode.begin(),Stk_type_lib[strokes[i]].unicode.end());
				last_cons=Stk_type_lib[strokes[i]].label;
			}
			break;	

		case LV:
		case RV:
		case MV:
			if(UAnuVis.size()==0)
			{
				if(Stk_type_lib[strokes[i]].label.compare("am_vm")==0 && i+1<strokes.size() && Stk_type_lib[strokes[i+1]].label.compare("am_vm")==0)
				{
					UAnuVis.push_back("0D03");
					i++;
					break;
				}
				else if(Stk_type_lib[strokes[i]].label.compare("dot")==0 && i+1<strokes.size() && Stk_type_lib[strokes[i+1]].label.compare("dot")==0)
				{
					UAnuVis.push_back("0D03");
					i++;
					break;
				}
				else if(Stk_type_lib[strokes[i]].label.compare("am_vm")==0)
				{
					UAnuVis=get_unicode("am_vm");
					break;
				}
				else if(Stk_type_lib[strokes[i]].label.compare("dot")==0)
				{
					UAnuVis=get_unicode("dot");
					break;
				}
			}
			if(UVM.size()== 0)
			{
				UVM=Stk_type_lib[strokes[i]].unicode;
				temp_vowel=Stk_type_lib[strokes[i]].label;
			}
			else if(UVM.size()==1)
			{	
				bool check_flag=false;
				for(unsigned int j=0;j<Stk_comp_lib.size();j++)
				{
					if((Stk_comp_lib[j].type==MV || Stk_comp_lib[j].type==RV) && temp_vowel.compare(Stk_comp_lib[j].sub1)==0 && Stk_type_lib[strokes[i]].label.compare(Stk_comp_lib[j].sub2)==0)
					{	
						UVM=Stk_comp_lib[j].unicode;
						check_flag=true;
						break;
					}
				}
				if(!check_flag)
					return 0;
			}
			else	
				return 0;
			break;
		case LC:
		case RC:
			if(UCM.size()==0)
			{
				UCM=get_unicode("chandrakkala");
				UCM.insert(UCM.end(),Stk_type_lib[strokes[i]].unicode.begin(),Stk_type_lib[strokes[i]].unicode.end());
			}
			else
				return 0;
			break;
		case MC:
			if(i==strokes.size()-1 && Stk_type_lib[strokes[i]].label.compare("chandrakkala")==0)
				app_ck=true;
			break;
		case W:
			//cout<<"Illegal Operation: W Reached unicode generate"<<endl;
			break;
		}
	}
	
	output.clear();
	if(UV.size()!=0)
		output=UV;
	else
		output=UC;
	if(UCM.size()!=0)
		output.insert(output.end(),UCM.begin(),UCM.end());
	if(UVM.size()!=0)
		output.insert(output.end(),UVM.begin(),UVM.end());
	if(UAnuVis.size()!=0)
		output.insert(output.end(),UAnuVis.begin(),UAnuVis.end());
	if(app_ck)
	{
		vector <string> tmp_ck=get_unicode("chandrakkala");
		output.insert(output.end(),tmp_ck.begin(),tmp_ck.end());
	}
	//cout<<"Unicode generated"<<output.size();
	return 1;
}

int get_stroke_size(vector<double> X, vector<double>Y)
{
	double x= *max_element(X.begin(),X.end())-*min_element(X.begin(),X.end());
	double y= *max_element(Y.begin(),Y.end())-*min_element(Y.begin(),Y.end());
	return int(x*y);
}
int PostProcess::Process(Stroke &Stk,int Display,vector<vector<string>  >&displaybuff,int topN,string iiit_diction_path,vector<stroke_type> &Stk_type_lib,vector<comp_stroke> &Stk_comp_lib, unsigned int beg_s, unsigned int end_s)
{
	this->Stk_type_lib=Stk_type_lib;
	this->Stk_comp_lib=Stk_comp_lib;
	if(!Display)
	{
		stroke_info temp_stk;
		temp_stk.ClassLabel.clear();
		temp_stk.ClassProb.clear();
		temp_stk.X.clear();
		temp_stk.Y.clear();


		// Size of current stroke
		int temp_size=get_stroke_size(Stk.original.x,Stk.original.y);

		// Remember the size of the largest stroke encountered till now.
		max_size=temp_size>max_size?temp_size:max_size;

		// Check for very small strokes and label it as .
		if(temp_size>10)
		{
			for(int i=0;i<(int)Stk.label.size();i++)
			{
				temp_stk.ClassLabel.push_back(Stk.label[i]);
				temp_stk.ClassProb.push_back(Stk.probability[i]);
			}
		}
		else
		{
			temp_stk.ClassLabel.push_back(int(144)); // 144 is the index of the label: dot.
			temp_stk.ClassProb.push_back(1);
		}

		// Decide between Tha, am_vm and 0 based on condition: If current stroke is smaller
		// than 1/4th of largest stroke encountered till now, relabel it appropriately.
		for(unsigned int i=0;temp_stk.ClassLabel.size()>1 && i<Stk.label.size();i++)
		{
			if((Stk_type_lib[temp_stk.ClassLabel[i]].label.compare("Tha")==0 && 4*temp_size < max_size) ||
			   (Stk_type_lib[temp_stk.ClassLabel[i]].label.compare("0")==0 && 4*temp_size < max_size))
			{
				for(unsigned int j=i+1;j<temp_stk.ClassLabel.size();j++)
					if(Stk_type_lib[temp_stk.ClassLabel[i]].label.compare("am_vm"))
					{
						int temp=temp_stk.ClassLabel[i];
						temp_stk.ClassLabel[i]=temp_stk.ClassLabel[j];
						temp_stk.ClassLabel[j]=temp;
					}
				break;
			}
			if((Stk_type_lib[temp_stk.ClassLabel[i]].label.compare("am_vm")==0 && 4*temp_size >= max_size) ||
			   (Stk_type_lib[temp_stk.ClassLabel[i]].label.compare("0")==0 && 4*temp_size >= max_size))
			{
				for(unsigned int j=i+1;j<temp_stk.ClassLabel.size();j++)
					if(Stk_type_lib[temp_stk.ClassLabel[i]].label.compare("Tha"))
					{
						int temp=temp_stk.ClassLabel[i];
						temp_stk.ClassLabel[i]=temp_stk.ClassLabel[j];
						temp_stk.ClassLabel[j]=temp;
					}
				break;
			}
		}

		temp_stk.X=Stk.original.x;
		temp_stk.Y=Stk.original.y;
		Stk_coll.push_back(temp_stk);


		//cout<<"checking"<<endl;
		//for(int i11=(int)beg_s;i11<(int)end_s;i11++)
		//{
			//cout<<"i11: "<<i11<<endl;
		for(int j=0;j<(int)Stk.label.size();j++)
				//cout<<Stk.label[j]<<" "<<Stk.probability[j]<<endl;
				//cout<<Stk_coll[i11].ClassLabel[j]<<" "<<Stk_coll[i11].ClassProb[j]<<endl;
			//cout<<endl;
		//}
		return 0;

	}
	else
	{
		/*if(beg_s==0)
		{
			for(int i=0;i<(int)displaybuff.size();i++)
				displaybuff[i].clear();
			displaybuff.clear();
		}*/
		// HACK: If the most likely label of the first character of the word is La (52), change it to i (2).
		if(Stk_coll[0].ClassLabel[0] == 52)
			Stk_coll[0].ClassLabel[0] = 2;
		akshara_startp.clear();
		string temp_diction=iiit_diction_path;
		temp_diction+="data\\config.txt";
		ifstream file_fp(temp_diction.c_str());
		file_fp>>seg_thresold;
		file_fp>>total_output;
		file_fp.close();

		size_t no_stroke=end_s;
		//cout<<"... Top 5 Stroke probability ...."<<endl<<endl;

		// Sort the strokes according to x position
		for(unsigned int i=beg_s;i<no_stroke;i++)
		for(unsigned int j=i+1;j<no_stroke;j++)
		{
				int t_1=(int)*min_element(Stk_coll[i].X.begin(),Stk_coll[i].X.end());
				int t_2=(int)*max_element(Stk_coll[i].X.begin(),Stk_coll[i].X.end());
				// If the previous stroke is sufficiently to the right of current stroke (no more than 30% overlap) swap the strokes.
				if(t_1 > *min_element(Stk_coll[j].X.begin(),Stk_coll[j].X.end()) && t_1+0.3*(t_2-t_1)> *max_element(Stk_coll[j].X.begin(),Stk_coll[j].X.end()))
				{
					stroke_info temp_copy=Stk_coll[i];
					Stk_coll[i]=Stk_coll[j];
					Stk_coll[j]=temp_copy;
				}
		}


		for(unsigned int i=beg_s;i<no_stroke;i++)
		{
			if(i>0)
			{
				int t_1=(int)*min_element(Stk_coll[i-1].X.begin(),Stk_coll[i-1].X.end());
				int t_2=(int)*max_element(Stk_coll[i-1].X.begin(),Stk_coll[i-1].X.end());
				if(t_1 > *min_element(Stk_coll[i].X.begin(),Stk_coll[i].X.end()) && t_1+0.3*(t_2-t_1)> *max_element(Stk_coll[i].X.begin(),Stk_coll[i].X.end()))
				{
					stroke_info temp_copy=Stk_coll[i-1];
					Stk_coll[i-1]=Stk_coll[i];
					Stk_coll[i]=temp_copy;
				}

			}
			temp_stroke.clear();
			size_t no_class=Stk_coll[i].ClassProb.size();
			for(unsigned int j=0;j<no_class;j++)
			{
				struct stroke_sort dummy;
				dummy.index=Stk_coll[i].ClassLabel[j];
				dummy.prob=Stk_coll[i].ClassProb[j];
				temp_stroke.push_back(dummy);
			}
			sort(temp_stroke.begin(),temp_stroke.end(),cmp_prob);
		}

		//doing segmentation here...
		//linear segmentation...
		
		// FSA for label disambiguation and UNICODE generation
		stk_state curr_seg_state=S;   // S: Start state

		unsigned int curr_stroke=0;   // Current stroke being processed.

		// This is a stack which stores the current interpretations of the aksharas till now,
		// along with the state in which the akshara was seen (both in stroke_state).
		stack <stroke_state> curr_akshara;
		string tmp_st;
		bool tilde_flag=false;

		// Length end-beg+1; initialized to 0.
		// Note that Stk_coll contains a list of stroke infos corresponding to each stroke.
		// Stk_coll_pos gives the index of the currently considered label of each stroke.
		vector <unsigned int> Stk_coll_pos(end_s-beg_s+1,0);  

		vector <string> temp_vec;
		// displaybuff is the output buffer containing a sequence of hexadecimal codes, each code is stored as a string.
		// Currently pushing an empty string, which will later be filled.??
		displaybuff.push_back(temp_vec);

		vector<string> temp_str;
		int min_A,max_A,min_B,max_B;
		while(curr_seg_state!=E && curr_stroke <= end_s)  // E: End state, which causes return.
		{
			int pos_list=Stk_coll_pos[curr_stroke];   // Take the currently considered label index of curr_stroke
			// Find the type of this stroke (or label): SV, SC, etc.
			int curr_stroke_type=curr_stroke<end_s?Stk_type_lib[Stk_coll[curr_stroke].ClassLabel[pos_list]].type:W;

			// BEGIN: Debug output
			//if(curr_stroke<end_s)
				//cout<<"stroke:"<<Stk_coll[curr_stroke].ClassLabel[pos_list]<<endl;
			//else
				//cout<<"stroke:W"<<endl;
			// END: Debug Output

			switch(curr_seg_state)
			{
			case S: 
				switch(curr_stroke_type)
				{
				case SC:
					curr_akshara.push(stroke_state(Stk_coll[curr_stroke++].ClassLabel[pos_list],S));
					curr_seg_state=A;
					break;
				case SV:
					curr_akshara.push(stroke_state(Stk_coll[curr_stroke++].ClassLabel[pos_list],S));
					curr_seg_state=V;
					break;
				case RC:
					curr_seg_state=AP;   // Corrected
					break;
				case RV:
					if (Stk_coll[curr_stroke].ClassLabel[pos_list]== 17) // If it is 'e_vm' an SV of e is possible.
					{
						curr_akshara.push(stroke_state(Stk_coll[curr_stroke++].ClassLabel[pos_list],S));
						curr_seg_state=S;
					}
					else
						curr_seg_state=AP;   // Corrected
					break;
				case LC:
				case LV:
				case MC:
				case MV:
					// Segmentation state remains as S, and we find the next interpretation of the curr_stroke.
					// If all interpretations are invalid, we skip the stroke.
					if(Stk_coll_pos[curr_stroke]< Stk_coll[curr_stroke].ClassLabel.size()-1)
						Stk_coll_pos[curr_stroke]++;
					else
						curr_stroke++;
					break;
				case W:
					curr_stroke++;
					curr_seg_state=E;  // To complete the FSA
					break;
				}
				break;
			case A:
				switch(curr_stroke_type)
				{
				case SC:
					// Find the min and max extents of the current stroke, A and previous stroke, B
					min_A=(int)*min_element(Stk_coll[curr_stroke].X.begin(),Stk_coll[curr_stroke].X.end());
					max_A=(int)*max_element(Stk_coll[curr_stroke].X.begin(),Stk_coll[curr_stroke].X.end());
					min_B=(int)*min_element(Stk_coll[curr_stroke-1].X.begin(),Stk_coll[curr_stroke-1].X.end());
					max_B=(int)*max_element(Stk_coll[curr_stroke-1].X.begin(),Stk_coll[curr_stroke-1].X.end());

					// If overlap is greater than smaller_stroke_size / 2
					if(max_B-min_A>(max_A-min_A<max_B-min_B?max_A-min_A:max_B-min_B)/2)
					{
						curr_akshara.push(stroke_state(Stk_coll[curr_stroke++].ClassLabel[pos_list],A));
						curr_seg_state=A;
					}
					else
					{
						// If we can generate a valid unicode sequence for the akshara till now.
						if(unicode_generate(curr_akshara,temp_str))
						{
							displaybuff[0].insert(displaybuff[0].end(),temp_str.begin(),temp_str.end());  // Append unicode to displaybuff
							while(!curr_akshara.empty())curr_akshara.pop();								  // Clear current_akshara stack
							curr_akshara.push(stroke_state(Stk_coll[curr_stroke++].ClassLabel[pos_list],A));  // Current SC begins a new akshara
							curr_seg_state=A;
						}
						else
							backtrack(curr_stroke,curr_seg_state,Stk_coll_pos,curr_akshara,tilde_flag,end_s);  // If not, re-interpret what is seen till now.
					}
					break;
				case SV:
					backtrack(curr_stroke,curr_seg_state,Stk_coll_pos,curr_akshara,tilde_flag,end_s);
					break;
				case RC:
				case RV:
					// If we can generate a valid unicode sequence for the akshara till now.
					// A new akshara with an RC/RV is starting here.
					if(unicode_generate(curr_akshara,temp_str))
					{
						displaybuff[0].insert(displaybuff[0].end(),temp_str.begin(),temp_str.end());
						while(!curr_akshara.empty())curr_akshara.pop();
						curr_akshara.push(stroke_state(Stk_coll[curr_stroke++].ClassLabel[pos_list],A));
						curr_seg_state=AP;
					}
					else
						backtrack(curr_stroke,curr_seg_state,Stk_coll_pos,curr_akshara,tilde_flag,end_s);
					break;
				case LC:
				case LV:
					curr_akshara.push(stroke_state(Stk_coll[curr_stroke++].ClassLabel[pos_list],A));
					curr_seg_state=A;
					break;
				case MC:
				case MV:
					curr_akshara.push(stroke_state(Stk_coll[curr_stroke++].ClassLabel[pos_list],A));
					tilde_flag=true;
					curr_seg_state=AP;
					break;
				case W:
					// End of word. Everything up till here should be a valid unicode sequence.
					if(unicode_generate(curr_akshara,temp_str))
					{
						displaybuff[0].insert(displaybuff[0].end(),temp_str.begin(),temp_str.end());
						while(!curr_akshara.empty())curr_akshara.pop();
						curr_stroke++;
						curr_seg_state=E;
					}
					else   // If not, re-interpret.
						backtrack(curr_stroke,curr_seg_state,Stk_coll_pos,curr_akshara,tilde_flag,end_s);
					break;
				}
				break;
			case AP:
				switch(curr_stroke_type)
				{
				case SC:
					curr_akshara.push(stroke_state(Stk_coll[curr_stroke++].ClassLabel[pos_list],AP));
					tilde_flag=false;
					curr_seg_state=A;
					break;
				case SV:
				case LC:
				case LV:
				case MC:
				case MV:
					backtrack(curr_stroke,curr_seg_state,Stk_coll_pos,curr_akshara,tilde_flag,end_s);
					break;
				case RC:
				case RV: 
					curr_akshara.push(stroke_state(Stk_coll[curr_stroke++].ClassLabel[pos_list],AP));
					tilde_flag=false;
					curr_seg_state=AP;
					break;
				case W:
					if(tilde_flag && unicode_generate(curr_akshara,temp_str))
					{
						displaybuff[0].insert(displaybuff[0].end(),temp_str.begin(),temp_str.end());
						while(!curr_akshara.empty())curr_akshara.pop();
						curr_stroke++;
						curr_seg_state=E;
					}
					else
						backtrack(curr_stroke,curr_seg_state,Stk_coll_pos,curr_akshara,tilde_flag,end_s);
					//cout<<"Current_stroke now:"<<curr_stroke<<"with seg_state"<<curr_seg_state<<endl;
					break;
				}
				break;
			case V:
				switch(curr_stroke_type)
				{
				case SC:
					if(unicode_generate(curr_akshara,temp_str))
					{
						displaybuff[0].insert(displaybuff[0].end(),temp_str.begin(),temp_str.end());
						while(!curr_akshara.empty())curr_akshara.pop();
						curr_akshara.push(stroke_state(Stk_coll[curr_stroke++].ClassLabel[pos_list],V));
						curr_seg_state=A;
					}
					else
						backtrack(curr_stroke,curr_seg_state,Stk_coll_pos,curr_akshara,tilde_flag,end_s);
					break;
				case SV:
				case MC:
				case MV:
					backtrack(curr_stroke,curr_seg_state,Stk_coll_pos,curr_akshara,tilde_flag,end_s);
					break;
				case RC:
				case RV:
					if(unicode_generate(curr_akshara,temp_str))
					{
						displaybuff[0].insert(displaybuff[0].end(),temp_str.begin(),temp_str.end());
						while(!curr_akshara.empty())curr_akshara.pop();
						curr_akshara.push(stroke_state(Stk_coll[curr_stroke++].ClassLabel[pos_list],V));
						curr_seg_state=AP;
					}
					else
						backtrack(curr_stroke,curr_seg_state,Stk_coll_pos,curr_akshara,tilde_flag,end_s);
					break;
				case LC:
				case LV:
					tmp_st=Stk_type_lib[Stk_coll[curr_stroke].ClassLabel[pos_list]].label;
					if(tmp_st.compare("A_vm")==0 || tmp_st.compare("au_vm")==0 || tmp_st.compare("am_vm")==0)
						curr_akshara.push(stroke_state(Stk_coll[curr_stroke++].ClassLabel[pos_list],V));
					else
						backtrack(curr_stroke,curr_seg_state,Stk_coll_pos,curr_akshara,tilde_flag,end_s);
					break;
				case W:
					if(unicode_generate(curr_akshara,temp_str))
					{
						displaybuff[0].insert(displaybuff[0].end(),temp_str.begin(),temp_str.end());
						while(!curr_akshara.empty())curr_akshara.pop();
						curr_stroke++;
						curr_seg_state=E;
					}
					else
						backtrack(curr_stroke,curr_seg_state,Stk_coll_pos,curr_akshara,tilde_flag,end_s);
					break;
				}
				break;
			default:
				cout<<"Illegal Segmentation state"<<endl;
			}
		}
		//for(unsigned int i=0;i<displaybuff[0].size();i++)
		//{
			//cout<<i<<":"<<displaybuff[0][i]<<endl;
		//}
		return(1);
	}

}
int PostProcess::ProcessDigit(Stroke &Stk,int Display,vector<vector<string>  >&displaybuff,int topN,string iiit_diction_path,vector<Akshara> &akshara_lib,vector<int> &Stroke_pos_info,unsigned int beg_s,unsigned int end_s)
{
	total_output=topN;
	if(!Display)
	{
		stroke_info temp_stk;
		temp_stk.ClassLabel.clear();
		temp_stk.ClassProb.clear();
		temp_stk.X.clear();
		temp_stk.Y.clear();


		for(unsigned int i=beg_s;i<end_s;i++)
		{
			temp_stk.ClassLabel.push_back(Stk.label[i]);
			temp_stk.ClassProb.push_back(Stk.probability[i]);
		}
		temp_stk.X=Stk.original.x;
		temp_stk.Y=Stk.original.y;
		Stk_coll.push_back(temp_stk);
	}
	else
	{
		/*if(beg_s==0)
		{
			for(int i=0;i<(int)displaybuff.size();i++)
				displaybuff[i].clear();
			displaybuff.clear();
		}*/
		//cout<<"Temp Poistion : 7"<<endl;
		double class_maxx=-1;
		string temp_diction=iiit_diction_path;
		temp_diction+="data\\config.txt";
		ifstream file_fp(temp_diction.c_str());
		file_fp>>seg_thresold;
		file_fp.close();
		total_output=topN;
		size_t no_stroke=end_s;
		//cout<<"... Top 5 Stroke probability ...."<<endl<<endl;

		/// SEEMS REDUNDANT
		//for(unsigned int i=beg_s;i<end_s;i++)
		//{
		//	temp_stroke.clear();
		//	size_t no_class=Stk_coll[i].ClassProb.size();
		//	for(unsigned int j=0;j<no_class;j++)
		//	{
		//		struct stroke_sort dummy;
		//		dummy.index=Stk_coll[i].ClassLabel[j];
		//		dummy.prob=Stk_coll[i].ClassProb[j];
		//		temp_stroke.push_back(dummy);
		//	}
		//}

		//doing segmentation here...
		//linear segmentation...
		akshara_startp.push_back(beg_s);
		class_maxx=*max_element(Stk_coll[beg_s].X.begin(),Stk_coll[beg_s].X.end());

		for(int i=(int)beg_s+1;i<(int)end_s;i++)
		{
			//cout<<"Temp Poistion : 8"<<endl;
			double curr_class_minx;
			curr_class_minx=*min_element(Stk_coll[i].X.begin(),Stk_coll[i].X.end());
			if((curr_class_minx-class_maxx)>seg_thresold)
			{
				class_maxx=-1;
			}
			else
			{
				double curr_class_maxx;
				curr_class_maxx=*max_element(Stk_coll[i].X.begin(),Stk_coll[i].X.end());
				if(curr_class_maxx>class_maxx)
					class_maxx=curr_class_maxx;
			}
			if(class_maxx==-1)
			{
				akshara_startp.push_back(i);
				class_maxx=*max_element(Stk_coll[i].X.begin(),Stk_coll[i].X.end());
			}
		}	
		akshara_startp.push_back(int(end_s));
		//cout<<"after positional segmentation"<<endl;
		//positional segmentation...
		//discuss with sir before completion..
		for(int i=(int)beg_s;i<(int)end_s;i++)
		{
			//cout<<"Temp Poistion : 9"<<endl;
			//cout<<"i: "<<i<<endl;
			for(int j=0;j<(int)akshara_startp.size()-1;j++)
			{
				//cout<<"startp: "<<akshara_startp[j]<<"  endp: "<<akshara_startp[j+1];
			}
			//cout<<endl;
			for(int j=0;j<(int)Stroke_pos_info.size()-1;j++)
			{
				//cout<<"Temp Poistion : 11"<<endl;
				//cout<<Stroke_pos_info.size()<<endl;
				//cout<<Stk_coll[i].ClassLabel[0]<<endl;
				if(Stroke_pos_info[Stk_coll[i].ClassLabel[0]]==1)	//next case..
				{
					//cout<<"Temp Poistion : 12"<<endl;
					//cout<<"next comes"<<endl;
					for(int k=0;k<(int)akshara_startp.size();k++)
					{
						
						if(akshara_startp[k]==i+1)
						{
							if(int(akshara_startp.size())>k+1)
								akshara_startp[k]=akshara_startp[k+1];
							else
								akshara_startp[k]=i+2;
							break;
						}		
					}
					break;
				}
				else if(Stroke_pos_info[Stk_coll[i].ClassLabel[0]]==2)  //prev case..
				{
					//cout<<"Temp Poistion : 13"<<endl;
					//cout<<"prev comes"<<endl;
					for(int k=0;k<(int)akshara_startp.size();k++)
					{
						if((akshara_startp[k]==i)&&(i!=0))
						{
							if(k!=0)
								akshara_startp[k]=akshara_startp[k-1];
							else
								akshara_startp[k]=i-1;
						}
					}
					break;
				}
				else if(Stroke_pos_info[Stk_coll[i].ClassLabel[0]]==3) //both
				{
					//cout<<"Temp Poistion : 14"<<endl;
					//cout<<"both  comes"<<endl;
					for(int k=0;k<(int)akshara_startp.size();k++)
					{
						if((akshara_startp[k]==i)&&(i!=0))
						{
							if(k!=0)
								akshara_startp[k]=akshara_startp[k-1];
							else
								akshara_startp[k]=i-1;
						}
					}
					for(int k=0;k<(int)akshara_startp.size();k++)
					{
						if(akshara_startp[k]==i+1)
						{
							if(int(akshara_startp.size())>k+1)
								akshara_startp[k]=akshara_startp[k+1];
							else
								akshara_startp[k]=i+2;
							break;
						}
					}
					break;

				}
			}
		}		
		for(int j=0;j<(int)akshara_startp.size()-1;j++)
		{
			//cout<<"startp: "<<akshara_startp[j]<<"  endp: "<<akshara_startp[j+1];
		}
		//cout<<endl;

		//displaybuff.clear();
		//cout<<"akshara_startp.size(): "<<akshara_startp.size()<<endl;
		for(int i=0;i<(int)akshara_startp.size()-1;i++)
		{
			//cout<<"Temp Poistion : 10"<<endl;
			//cout<<"startp: "<<akshara_startp[i]<<"  endp: "<<akshara_startp[i+1];
			if((akshara_startp[i+1]>akshara_startp[i])&&(akshara_startp[i]<(int)Stk_coll.size()))
			{
				if(akshara_startp[i+1]>=(int)end_s)
					akshara_startp[i+1]=int(end_s);

				Recognize(displaybuff,akshara_startp[i],akshara_startp[i+1],akshara_lib);    //the recognition routine calls here....
			}
		}
		//cout<<"in post:"<<displaybuff.size()<<endl;
		for(int i=(int)beg_s;i<(int)end_s;i++)
		{
			Stk_coll[i].ClassLabel.clear();
			Stk_coll[i].ClassProb.clear();
			Stk_coll[i].X.clear();
			Stk_coll[i].Y.clear();
		}
		if(Stk_coll.size()==end_s)
			Stk_coll.clear();
	}
	return(1);
}
bool isTextOnly(int classLabel)
{
	if(classLabel<TEXT_THRES)
		return true;
	else
		return false;
}
int PostProcess::ProcessMixed(Stroke &Stk,int Display,vector<vector<string>  >&displaybuff,int topN,string iiit_diction_path,vector<stroke_type> &Stk_type_lib,vector<comp_stroke> &Stk_comp_lib,vector<Akshara> &akshara_lib,vector<int> &Stroke_pos_info)
{
	if(!Display)
		Process(Stk,Display,displaybuff,topN,iiit_diction_path,Stk_type_lib,Stk_comp_lib,0,1);
	else
	{
	
		temp_carrier.clear();

		for(unsigned int i=0;i<Stk_coll.size();i++)
			temp_carrier.push_back(Stk_coll[i]);
		for(unsigned int i=0;i<Stk_coll.size();i++)
		{
			Stk_coll[i].ClassLabel.clear();
			Stk_coll[i].ClassProb.clear();
			Stk_coll[i].X.clear();
			Stk_coll[i].Y.clear();
		}
			Stk_coll.clear();

		bool current=isTextOnly(temp_carrier[0].ClassLabel[0]);
		int beg=0;
		for(unsigned int i=0;i<temp_carrier.size();i++)
		{
			/*if(i>0 && i<temp_carrier.size()-1 && !isTextOnly(temp_carrier[i-1].ClassLabel[0]) && !isTextOnly(temp_carrier[i+1].ClassLabel[0]) && temp_carrier[i].ClassLabel.size()>1 && isTextOnly(temp_carrier[i].ClassLabel[0]) && !isTextOnly(temp_carrier[i].ClassLabel[1]))
			{
				int temp=temp_carrier[i].ClassLabel[0];
				temp_carrier[i].ClassLabel[0]=temp_carrier[i].ClassLabel[1];
				temp_carrier[i].ClassLabel[1]=temp;
			}*/

			if(current!=isTextOnly(temp_carrier[i].ClassLabel[0]))
			{
				if(current)
				{
					for(unsigned int j=0;j<Stk_coll.size();j++)
					{
						Stk_coll[j].ClassLabel.clear();
						Stk_coll[j].ClassProb.clear();
						Stk_coll[j].X.clear();
						Stk_coll[j].Y.clear();
					}
					Stk_coll.clear();
					for(unsigned int j=beg;j<i;j++)
					{
						

						stroke_info temp_stk;
						temp_stk.ClassLabel.clear();
						temp_stk.ClassProb.clear();
						temp_stk.X.clear();
						temp_stk.Y.clear();


						for(unsigned int k=0;k<temp_carrier[j].ClassLabel.size();k++)
						{
							if(temp_carrier[j].ClassLabel[k]>130)  //// SPT - Changed from 90 to 110
								continue;
							temp_stk.ClassLabel.push_back(temp_carrier[j].ClassLabel[k]);
							temp_stk.ClassProb.push_back(temp_carrier[j].ClassProb[k]);
						}
						temp_stk.X=temp_carrier[j].X;
						temp_stk.Y=temp_carrier[j].Y;
						Stk_coll.push_back(temp_stk);
					}
					Process(Stk,Display,displaybuff,topN,iiit_diction_path,Stk_type_lib,Stk_comp_lib,0,(int)Stk_coll.size());					
				}
				else
				{
					for(unsigned int j=0;j<Stk_coll.size();j++)
					{
						Stk_coll[j].ClassLabel.clear();
						Stk_coll[j].ClassProb.clear();
						Stk_coll[j].X.clear();
						Stk_coll[j].Y.clear();
					}
					Stk_coll.clear();
					for(unsigned int j=beg;j<i;j++)
					{
						

						stroke_info temp_stk;
						temp_stk.ClassLabel.clear();
						temp_stk.ClassProb.clear();
						temp_stk.X.clear();
						temp_stk.Y.clear();


						for(unsigned int k=0;k<temp_carrier[j].ClassLabel.size();k++)
						{
							if(temp_carrier[j].ClassLabel[k]<130) //// SPT - Changed from 90 to 130
								continue;
							temp_stk.ClassLabel.push_back(temp_carrier[j].ClassLabel[k]-90);
							temp_stk.ClassProb.push_back(temp_carrier[j].ClassProb[k]);
						}
						temp_stk.X=temp_carrier[j].X;
						temp_stk.Y=temp_carrier[j].Y;
						Stk_coll.push_back(temp_stk);
					}
					ProcessDigit(Stk,Display,displaybuff,topN,iiit_diction_path,akshara_lib,Stroke_pos_info,0,(int)Stk_coll.size());
				}
				beg=i;
				current= !current;
			}
			else if(i==temp_carrier.size()-1)
			{
				i++;
				if(current)
				{
					for(unsigned int j=0;j<Stk_coll.size();j++)
					{
						Stk_coll[j].ClassLabel.clear();
						Stk_coll[j].ClassProb.clear();
						Stk_coll[j].X.clear();
						Stk_coll[j].Y.clear();
					}
					Stk_coll.clear();
					for(unsigned int j=beg;j<i;j++)
					{
						

						stroke_info temp_stk;
						temp_stk.ClassLabel.clear();
						temp_stk.ClassProb.clear();
						temp_stk.X.clear();
						temp_stk.Y.clear();


						for(unsigned int k=0;k<temp_carrier[j].ClassLabel.size();k++)
						{
							if(temp_carrier[j].ClassLabel[k]>130)
								continue;
							temp_stk.ClassLabel.push_back(temp_carrier[j].ClassLabel[k]);
							temp_stk.ClassProb.push_back(temp_carrier[j].ClassProb[k]);
						}
						temp_stk.X=temp_carrier[j].X;
						temp_stk.Y=temp_carrier[j].Y;
						Stk_coll.push_back(temp_stk);
					}
					Process(Stk,Display,displaybuff,topN,iiit_diction_path,Stk_type_lib,Stk_comp_lib,0,(int)Stk_coll.size());
					
				}
				else
				{
					for(unsigned int j=0;j<Stk_coll.size();j++)
					{
						Stk_coll[j].ClassLabel.clear();
						Stk_coll[j].ClassProb.clear();
						Stk_coll[j].X.clear();
						Stk_coll[j].Y.clear();
					}
					Stk_coll.clear();
					for(unsigned int j=beg;j<i;j++)
					{
						

						stroke_info temp_stk;
						temp_stk.ClassLabel.clear();
						temp_stk.ClassProb.clear();
						temp_stk.X.clear();
						temp_stk.Y.clear();


						for(unsigned int k=0;k<temp_carrier[j].ClassLabel.size();k++)
						{
							if(temp_carrier[j].ClassLabel[k]<130)
								continue;
							temp_stk.ClassLabel.push_back(temp_carrier[j].ClassLabel[k]-90);
							temp_stk.ClassProb.push_back(temp_carrier[j].ClassProb[k]);
						}
						temp_stk.X=temp_carrier[j].X;
						temp_stk.Y=temp_carrier[j].Y;
						Stk_coll.push_back(temp_stk);
					}
					ProcessDigit(Stk,Display,displaybuff,topN,iiit_diction_path,akshara_lib,Stroke_pos_info,0,(int)Stk_coll.size());
				}
				beg=i;
				current= !current;
			}
		}
	}
	return (1);
}


int post_cmp(const void *a,const  void *b ) 
{
	post_labels *s1,*s2;
	s1=(post_labels*)a;
	s2=(post_labels*)b;
	if(s1->prob > s2->prob)
		return -1;
	else
		return 1;
	//      return a > b;
} 
int PostProcess::Recognize(vector<vector<string> >&displaybuff,int start_pos,int end_pos,vector<Akshara> &akshara_lib)
{
	post_labels *akshara_prob;
	akshara_prob=(post_labels*)malloc(sizeof(post_labels)*(int)akshara_lib.size());
	int flag=1;
	for(int i=0;i<(int)akshara_lib.size();i++)
	{
		double min_prob=0;
		flag=1;
		for(int j=0;j<(int)akshara_lib[i].stroke_seq.size();j++)
		{
			double temp_prob=0;
			if((end_pos-start_pos)!=(int)akshara_lib[i].stroke_seq[j].size())
				temp_prob=-100000;
			else
			{		
				for(int k=0;k<(int)akshara_lib[i].stroke_seq[j].size();k++)
				{
					//temp_prob+=ClassProb[k][akshara_lib[i].stroke_seq[j][k]];
					for(int l=0;l<(int)Stk_coll[k+start_pos].ClassLabel.size();l++)
					{
						if(Stk_coll[k+start_pos].ClassLabel[l]==akshara_lib[i].stroke_seq[j][k])
						{
							//cout<<"matching found"<<ClassLabel[k][l]<<" "<<akshara_lib[i].stroke_seq[j][k]<<" i:"<<i<<"  j:"<<j<<" k:"<<k<<" l:"<<l<<endl;
							temp_prob+=Stk_coll[k+start_pos].ClassProb[l];
							flag=0;
						}
					}
					if(flag==1)
						temp_prob+=-1;
					flag=1;	
					/*					for(int l=0;l<(int)ClassLabel[k].size();l++)
					{
					if(ClassLabel[k][l]==akshara_lib[i].stroke_seq[j][k])
					{
					//cout<<"matching found"<<ClassLabel[k][l]<<" "<<akshara_lib[i].stroke_seq[j][k]<<" i:"<<i<<"  j:"<<j<<" k:"<<k<<" l:"<<l<<endl;
					temp_prob+=ClassProb[k][l];
					flag=0;
					}
					}
					if(flag==1)
					temp_prob+=-10000;
					flag=1;*/
				}
				temp_prob/=(int)akshara_lib[i].stroke_seq[j].size();
			}
			//cout<<"temp_prob:"<<temp_prob<<" :"<<i<<endl;
			/*for(int k=0;k<(int)akshara_lib[i].unicode.size();k++)
			cout<<" "<<akshara_lib[i].unicode[k];
			cout<<"temp_prob"<<temp_prob<<" :"<<i<<endl;*/
			if(min_prob==0)
				min_prob=temp_prob;
			else if(min_prob<temp_prob)
				min_prob=temp_prob;
		}
		akshara_prob[i].index=i;
		akshara_prob[i].prob=min_prob;
	}
	//sort akshara_prob....
	/*for(int i=0;i<(int)akshara_lib.size();i++)
	{
	cout<<" "<<akshara_prob[i].index<<" "<<akshara_prob[i].prob<<endl;
	}*/
	qsort(akshara_prob, (int)akshara_lib.size(), sizeof(post_labels),post_cmp);
	/*cout<<"after sorting"<<endl;
	for(int i=0;i<(int)akshara_lib.size();i++)
	{
	cout<<" "<<akshara_prob[i].index<<" "<<akshara_prob[i].prob<<endl;
	}*/

	//cout<<"test2"<<endl;
	//cout<<" Top 5 Akshara probability\n\n\n";	
	//for(int i=0;i<total_output;i++)
	{
		int i=0;
		vector<string>temp_uni;
		temp_uni.clear();
		int top_i=akshara_prob[i].index;
		//cout<<top_i<<endl;
		//cout<<1<<": ";
		//cout<<akshara_lib[top_i].aksharaID<<" ";
		//cout<<akshara_prob[i].prob<<": ";
		double x_size=*max_element(Stk_coll[start_pos].X.begin(),Stk_coll[start_pos].X.end())-*min_element(Stk_coll[start_pos].X.begin(),Stk_coll[start_pos].X.end());
		double y_size=*max_element(Stk_coll[start_pos].Y.begin(),Stk_coll[start_pos].Y.end())-*min_element(Stk_coll[start_pos].Y.begin(),Stk_coll[start_pos].Y.end());
		if(y_size/x_size < 0.3 || y_size < 4)
			temp_uni.push_back("002D");
		else if(x_size/y_size <0.2)
			temp_uni.push_back("0031");
		else
		{
			for(int j=0;j<(int)akshara_lib[top_i].unicode.size();j++)
			{
				//cout<<akshara_lib[top_i].unicode[j]<<" ";
				temp_uni.push_back(akshara_lib[top_i].unicode[j]);
			}
		}
		//cout << endl;
		vector <string> temp_vec;
		displaybuff.push_back(temp_vec);
		displaybuff[0].insert(displaybuff[0].end(),temp_uni.begin(),temp_uni.end());
		//displaybuff.push_back(temp_uni);
	}
	free(akshara_prob);
	return 1;
}


void PostProcess::clearall(vector<vector<string> >&displaybuff)
{

	for(int i=0;i<(int)Stk_coll.size();i++)
	{
		Stk_coll[i].ClassLabel.clear();
		Stk_coll[i].ClassProb.clear();
		Stk_coll[i].X.clear();
		Stk_coll[i].Y.clear();
	}
	Stk_coll.clear();
	for(int i=0;i<(int)displaybuff.size();i++)
		displaybuff[i].clear();
	displaybuff.clear();
}
void PostProcess::save_log(vector<vector<string>  >&displaybuff,string log_filename)
{
	//log_filename+=".log";
	SYSTEMTIME st;
    GetLocalTime(&st);
	ofstream file_fp(log_filename.c_str(),ios::app);
	file_fp<<"Current time is: "<<st.wYear<<":"<<st.wMonth<<":"<<st.wDay<<" :"<<st.wHour<<" :"<<st.wMinute<<endl;
	file_fp<<"total_no_of_strokes: ";
	file_fp<<Stk_coll.size()<<endl;
	for(int i=0;i<(int)Stk_coll.size();i++)
	{
		file_fp<<"Stroke_no: ";
		file_fp<<i+1<<endl;
		file_fp<<"X\tY"<<endl;
		for(int j=0;j<(int)Stk_coll[i].X.size();j++)
		{
			file_fp<<Stk_coll[i].X[j]<<" "<<Stk_coll[i].Y[j]<<"  ";
		}
		file_fp<<endl;
		file_fp<<"ClassLabel\tClassProb"<<endl;
		for(int j=0;j<(int)Stk_coll[i].ClassLabel.size();j++)
		{
			file_fp<<j<<":"<<Stk_coll[i].ClassLabel[j]<<" "<<Stk_coll[i].ClassProb[j]<<"  ";
		}
		file_fp<<endl;
	}
	file_fp<<"Total no of aksharas: "<<displaybuff.size()/total_output<<endl;
	for(int i=0;i<(int)displaybuff.size();i++)
	{
		if(i%total_output==0)
			file_fp<<"akshara: "<<i/total_output+1<<endl;
		file_fp<<"preference: "<<i%total_output+1<<"  ";
		for(int j=0;j<(int)displaybuff[i].size();j++)
			file_fp<<displaybuff[i][j]<<"  ";
		file_fp<<endl;
	}
	file_fp.close();
}
