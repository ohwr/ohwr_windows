/**																
	\file sample1.cpp
  	\brief  only sample code 
	\details xxxxxxxx
  	\version 1.0.0.0
  	<b> All COPYRIGHTS ARE RESERVERD TO C-DAC GIST PUNE 2012 </b>
*/


#include "sample1.h" 

#ifdef __cplusplus
extern "C"
{  
#endif

		void fn_CalcAreaCircle(CIRCLE* d)
		{
			if(d!=NULL)
			{
				double iRad=(double)d->rad;	/**local var member used for ... */
				/**
					area of circle is PI * rad^2
				*/
				d->Area= PI* pow(iRad,2);
			}
		}

#ifdef __cplusplus
}  
#endif

