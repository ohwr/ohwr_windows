#ifndef _PRE_PROCESS_H
#define _PRE_PROCESS_H

#include "..\stroke\stroke.h"
class preProcess
{
	vector <double> X;
	vector <double> Y;
	vector <int> critPoints;
	
	int reverse_char();
	int smooth();
	int equiDistant_Resample(float);
	int fitToSpline();
	
	int computeCritPoints(void);
	int gaussFilter(int );
	double computeDistance(int ,int);
	int normalization(int);
	public:
	preProcess();
	~preProcess();
	
	int Process(Stroke &Stk,int Loc);	
};
#endif
