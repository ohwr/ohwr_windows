#ifndef _STROKE_H
#define _STROKE_H

#include<vector>
#include<iostream>
#include<cmath>
#include<string>
#include<fstream>
using namespace std;

//vector <int> DisplayBuff;
struct Lpoint
{
	vector <double> x;
	vector <double> y;
	vector <double> t;

	int xResolution;
	int yResolution;

	double pressure;
	int samplingRate;
};

struct FeatPoint
{
	vector <vector <double> > local;
	vector <double> global;
};	

class Stroke
{
	public:
	Lpoint 	original;
	Lpoint  processed;
	Lpoint equipoints;
	Lpoint midproc;
	FeatPoint feat;

	vector <int>  label;
	vector <double> probability;

	Stroke();
	~Stroke();
	
	int writeToRcardFile(ofstream &);// write original stroke points
	int readFromRcardFile(ifstream &);// Read original stroke only
};

class Akshra
{
	vector <class Stroke> akshar;
	string unicode;
	string language;
	
	Akshra();
	~Akshra();
};		
#endif
