/**																
\file RecognitionEngine.cpp
\brief DevRecognitionEngine.cpp contains definations of the  interfaces functions of the Application for Online Handwritten Character Recognition (OHWR) System
\date(Date of Creation)
\date  (Last modified)
\author xxxxx
\version 1.0.0.0
<b> All COPYRIGHTS ARE RESERVERD TO C-DAC GIST PUNE 2012 </b>
*/
#include "..\include\RecognitionEngine.h"
#include "..\src\Classifier\svm.h"
#include <string.h>
#include <fstream>
#include <locale>
#include <sstream>
#include <sstream>
#include <iostream>
#include <cwchar>
#include <cstdlib>



#ifdef __cplusplus
extern "C"
{  
#endif

	void getParaInfo(const FILE_STROKEINFO* , int, int*, int*, int*, int*, int [][20], int [][20] );
	double findWordThreshold(const FILE_STROKEINFO* , int);
	int getNLines(int*, int);

	vector<Akshara> iiit_akshara_lib;
	vector<string> iiit_Stroke_map;
	vector<int> iiit_Stroke_pos_info;
	vector<stroke_type> iiit_Stk_type_lib;
	vector<comp_stroke> iiit_Stk_comp_lib;
	vector<Akshara> iiit_digit_lib;
	vector<string> iiit_digitstk_map;
	vector<int> iiit_digitstk_pos_info;
	string iiit_diction_path;
	string log_filename;
	string logname_cdac;
	double iiit_scale_min;
	double iiit_scale_max;
	vector<double>iiit_scale_minx;
	vector<double>iiit_scale_maxx;
	struct svm_model* model1;  //for text cases...
	struct svm_model* model2;  //for digits only...
	struct svm_model* model3; //for mixed case...

	//confusion matrix functions....
	vector<vector<int> >iiit_global_conf_class;
	vector<vector<double> >iiit_global_conf_prob;
	vector<vector<int> >iiit_mixed_conf_class;
	vector<vector<double> >iiit_mixed_conf_prob;
	vector<vector<int> >iiit_digit_conf_class;
	vector<vector<double> >iiit_digit_conf_prob;
	vector<vector<int>>iiit_ClassType;
	int Loc;
	int numberofDicts; 
	vector<vector<wstring> > iiit_Mal_dictionary;

	char*  g_strEngineDirectoryPath;
	char*  g_strWordListDirectoryPath;

	///SimString Initialization Of db
	string iiit_DicPath; ////Different from iiit_dictionary_path. This has dictionary lookup path. 


	// Initialization called at position 1
	// The structure iiit_ClassType is filled from the file ClassType.txt
	// iiit_ClassType contains the list of possible classes (ints) for each type of field
	// such as MIXED, PLAIN, NUM, ...
	int initialize_ClassType(){
		string temp_diction=iiit_diction_path;
		temp_diction+="data\\ClassType.txt";
		ifstream fi(temp_diction.c_str());
		if(fi.is_open()){
			int numlines;
			fi>>numlines;cout<<numlines<<endl;
			vector<vector<int> >Cl;
			int npinline;
			iiit_ClassType.clear();
			for (int i=0;i<(int)Cl.size();i++)
				iiit_ClassType[i].clear();
			vector<int> vv;
			for(int i=0;i<numlines;i++)
			{

				fi>>npinline;
				for(int j=0;j<npinline;j++)
				{
					int temp;fi>>temp;
					vv.push_back(temp);

				}
				iiit_ClassType.push_back(vv);
				vv.clear();
			}

		}
		return 0;
	}

	int initialize_Dictionary(){
		vector <int> numelinDic;numelinDic.clear();
		vector <wstring> tempDicelements;tempDicelements.clear();

		//dictionary.resize(5, vector<wstring>(7, L"") );
		iiit_Mal_dictionary.clear();
		for(int i=0;i<(int)iiit_Mal_dictionary.size();i++)
			iiit_Mal_dictionary[i].clear();
		//wstring tempDic(iiit_DicPath.begin(),iiit_DicPath.end());// = iiit_DicPath;
		string fa(iiit_DicPath.begin(),iiit_DicPath.end());
		fa+="Dictionary.txt";
		//tempDic.assign(fa.begin(),fa.end());
		//wstring tempDic(fa.length(),L' ');
		//copy(fa.begin(),fa.end(),tempDic.begin());
		//tempDic=L"."+tempDic;
		//tempDic = ws.str();
		//tempDic.append(L"Dictionary.txt");


		FILE *fs = fopen(fa.c_str(),"rb,ccs=UTF-16LE");
		wchar_t buffer[50];
		if(fs){
			////Reading the first line and the number of dictionaries. No use after fgetws can be discarded but let it be there for now.
			fgetws(buffer, sizeof(buffer), fs);
			const int len = wcslen(buffer);
			wchar_t *temp= (wchar_t *) malloc((len-1)*sizeof(wchar_t));
			wstringstream ss;
			wstring s;
			ss<<buffer;ss>>s;
			const wchar_t *cstr = s.c_str();
			for(int i=0;i<(int)wcslen(cstr);i++)
				temp[i] = cstr[i+1];
			int numberofDicts  = (int)_wtoi(temp);

			////Reading rest of the lines
			int LineNum=0;
			while (fgetws(buffer, sizeof(buffer), fs)) 
			{
				LineNum +=1;
				const int len = wcslen(buffer);
				//wstringstream ss;wstring s;
				//ss<<buffer;ss>>s;
				//const wchar_t *cstr = s.c_str();
				wchar_t *cstr = buffer;

				if(cstr[0] == L'#'){
					//dbw.close();
					wchar_t numBuffer[50]=L"";
					int j=1;
					for(;j<len;j++){
						if(cstr[j] == L':')
							break;
						numBuffer[j-1]=cstr[j];
					}
					wchar_t *numindicbuffer= (wchar_t *) malloc((len-j-1)*sizeof(wchar_t));
					for(int i=j+1;i<len;i++)
						numindicbuffer[i-j-1] = cstr[i];
					numelinDic.push_back(_wtoi(numindicbuffer));//Number of elements in that dictionary. used to read those lines.
				}
				else{

					wstring s2(buffer);
					tempDicelements.push_back(s2);
				}	
			}

			int index=0;vector<wstring>jaff,temphere;jaff.clear();temphere.clear();
			if ((int)accumulate(numelinDic.begin(),numelinDic.end(),0) == (int)tempDicelements.size()){
				for(int i=0;i<numberofDicts;i++){
					for(int j=0;j<numelinDic[i];j++){
						temphere.push_back(tempDicelements[index]);
						index+=1;
					}
					iiit_Mal_dictionary.push_back(temphere);temphere.clear();
				}
			}
			return 0;
		}
		else
			return -12;
	}

	// Initialization called at position 2
	// The structure iiit_Stroke_map is filled from the file MalayalamAllStrokes.txt
	// The structure iiit_digitstk_map is filled from the file DigitAllStrokes.txt
	// Both are vectors of strings, and contains the list of human readable class labels a, A, i ..
	// and the 14 classes: 0,1,..,9,half_5,-,<,|  for digitstk_map
	int initialize_stkmap()
	{
		string temp_diction=iiit_diction_path;
		temp_diction+="data\\MalayalamAllStrokes.txt";
		ifstream lib_fp(temp_diction.c_str());
		if(lib_fp.is_open())
		{
			string post_temp;
			lib_fp>>post_temp;
			while(!lib_fp.eof())
			{
				iiit_Stroke_map.push_back(post_temp);
				lib_fp>>post_temp;
			}
		}
		else return -1;
		lib_fp.close();
		temp_diction=iiit_diction_path;
		temp_diction+="data\\DigitAllStrokes.txt";
		ifstream lib_fp1(temp_diction.c_str());
		if(lib_fp1.is_open())
		{
			string post_temp;
			lib_fp1>>post_temp;
			while(!lib_fp1.eof())
			{
				iiit_digitstk_map.push_back(post_temp);
				lib_fp1>>post_temp;
			}
		}
		else return -2;
		lib_fp1.close();
		return 0;
	}

	stk_type get_stk_type(string str_type)
	{
		if(str_type.compare("SC")==0)
			return SC;
		else if(str_type.compare("SV")==0)
			return SV;
		else if(str_type.compare("LC")==0)
			return LC;
		else if(str_type.compare("LV")==0)
			return LV;
		else if(str_type.compare("RC")==0)
			return RC;
		else if(str_type.compare("RV")==0)
			return RV;
		else if(str_type.compare("MC")==0)
			return MC;
		else if(str_type.compare("MV")==0)
			return MV;		
		else
			cout<<"Invalid Stroke type!!";
		return W;	
	}


	int initialize_stktypelist()
	{
		string temp_diction=iiit_diction_path;
		temp_diction+="data\\StrokesUnicodes.txt";
		ifstream lib_fp(temp_diction.c_str());
		if(lib_fp.is_open())
		{
			string post_temp;
			lib_fp>>post_temp;
			while(!lib_fp.eof())
			{
				stroke_type temp_stk_type;
				temp_stk_type.label=post_temp;
				string str_type;
				lib_fp>>str_type;
				temp_stk_type.type=get_stk_type(str_type);
				int stk_unicode_no;
				lib_fp>>stk_unicode_no;
				string temp_unicode;
				for(int i=0;i<stk_unicode_no;i++)
				{
					lib_fp>>temp_unicode;
					temp_stk_type.unicode.push_back(temp_unicode.substr(1));
				}
				iiit_Stk_type_lib.push_back(temp_stk_type);
				lib_fp>>post_temp;
			}
		}
		else
			return -21;
		stroke_type temp_stk_type;
		temp_stk_type.label="dot";
		temp_stk_type.type=LV;
		temp_stk_type.unicode.push_back("002E");
		iiit_Stk_type_lib.push_back(temp_stk_type);
		lib_fp.close();

		temp_diction=iiit_diction_path;
		temp_diction+="data\\CompStrokes.txt";
		ifstream lib_fp1(temp_diction.c_str());
		if(lib_fp1.is_open())
		{
			string post_temp;
			lib_fp1>>post_temp;
			while(!lib_fp1.eof())
			{
				comp_stroke temp_stk_type;
				temp_stk_type.label=post_temp;
				string str_type;
				lib_fp1>>str_type;
				temp_stk_type.type=get_stk_type(str_type);
				int stk_unicode_no;
				lib_fp1>>stk_unicode_no;
				string temp_unicode;
				for(int i=0;i<stk_unicode_no;i++)
				{
					lib_fp1>>temp_unicode;
					temp_stk_type.unicode.push_back(temp_unicode.substr(1));
				}
				lib_fp1>>temp_stk_type.sub1;
				lib_fp1>>temp_stk_type.sub2;
				iiit_Stk_comp_lib.push_back(temp_stk_type);
				lib_fp1>>post_temp;
			}
		}
		else return -22;
		lib_fp1.close();
		return 0;
	}


	// Initialization called at position 4
	// For modifiers, store the assosciation in iiit_Stroke_pos_info vector<int>
	// Read from MalGroupingRules.txt
	// Similarly, iiit_digitstk_pos_info is filled from DigitGroupingRules.txt
	int initialize_stkpos()
	{
		//0 for normal strokes 1 for next 2 for prev 3 for both
		for(int i=0;i<(int)iiit_Stroke_map.size();i++)
			iiit_Stroke_pos_info.push_back(0);
		string temp_diction=iiit_diction_path;
		temp_diction+="data\\MalGroupingRules.txt";
		ifstream lib_fp(temp_diction.c_str());
		if(lib_fp.is_open())
		{
			string post_temp;
			while(!lib_fp.eof())
			{
				lib_fp>>post_temp;
				int curr_stroke_int=-1;
				for(int l=0;l<(int)iiit_Stroke_map.size();l++)
				{
					if(iiit_Stroke_map[l].compare(post_temp)==0)
					{
						curr_stroke_int=l;
						break;
					}
				}
				lib_fp>>post_temp;
				if(curr_stroke_int!=-1)
				{
					if(post_temp.compare(string("P"))==0)
					{
						cout<<"stroke: "<<curr_stroke_int<<endl;
						iiit_Stroke_pos_info[curr_stroke_int]=2;
					}
					else if(post_temp.compare(string("B"))==0)
						iiit_Stroke_pos_info[curr_stroke_int]=3;
					else if(post_temp.compare(string("N"))==0)
						iiit_Stroke_pos_info[curr_stroke_int]=1;	
				}
			}
		}
		else return -3;
		lib_fp.close();
		for(int i=0;i<(int)iiit_digitstk_map.size();i++)
			iiit_digitstk_pos_info.push_back(0);
		temp_diction=iiit_diction_path;
		temp_diction+="data\\DigitGroupingRules.txt";
		ifstream lib_fp1(temp_diction.c_str());
		if(lib_fp1.is_open())
		{
			string post_temp;
			while(!lib_fp1.eof())
			{
				lib_fp1>>post_temp;
				int curr_stroke_int=-1;
				for(int l=0;l<(int)iiit_digitstk_map.size();l++)
				{
					if(iiit_digitstk_map[l].compare(post_temp)==0)
					{
						curr_stroke_int=l;
						break;
					}
				}
				lib_fp1>>post_temp;
				if(curr_stroke_int!=-1)
				{
					if(post_temp.compare(string("P"))==0)
					{
						cout<<"stroke: "<<curr_stroke_int<<endl;
						iiit_digitstk_pos_info[curr_stroke_int]=2;
					}
					else if(post_temp.compare(string("B"))==0)
						iiit_digitstk_pos_info[curr_stroke_int]=3;
					else if(post_temp.compare(string("N"))==0)
						iiit_digitstk_pos_info[curr_stroke_int]=1;
				}	
			}
		}
		else return -4;
		lib_fp1.close();
		return 0;
	}

	// Initialization called at position 3
	// The structure iiit_akshara_lib is filled from the file MalAksharaStrokeMap.txt
	// The structure iiit_digit_lib is filled from the file DigitStrokeMap.txt
	// Both are vectors of Akshara, which contains: 1) String AksharaID, 2) vec<vec<int>> stroke_seq,
	// and 3) vec<string> unicode.
	int initialize_lib()
	{
		string temp_diction=iiit_diction_path;
		temp_diction+="data\\MalAksharaStrokeMap.txt";
		ifstream lib_fp(temp_diction.c_str());
		if(lib_fp.is_open())
		{
			string post_temp;
			int post_total_class;
			int post_total_stroke;
			lib_fp>>post_total_class;  // Number of Aksharas
			//	cout<<post_total_class<<endl;
			getline(lib_fp,post_temp);
			lib_fp>>post_total_stroke;  // Number of strokes
			getline(lib_fp,post_temp);
			//getline(lib_fp,post_temp);
			//getline(lib_fp,post_temp);
			for(int i=0;i<post_total_class;i++)  // Reah each akshara map
			{

				//cout<<i<<endl;
				Akshara temp_aksh;
				for(int j=0;j<(int)temp_aksh.stroke_seq.size();j++)
					temp_aksh.stroke_seq[j].clear();
				temp_aksh.stroke_seq.clear();
				temp_aksh.unicode.clear();
				//			getline(lib_fp,post_temp);

				lib_fp>>temp_aksh.aksharaID;  // Human readable akshara representation
				int numUnicodes;              // Number of codes in the unicode of current akshara
				lib_fp >> numUnicodes;
				for(int j=0;j<numUnicodes;j++)
				{
					string curr_unicode;
					lib_fp>>curr_unicode;
					curr_unicode=curr_unicode.substr(1);
					temp_aksh.unicode.push_back(curr_unicode);
				}

				//				int curr_class_type=1; //redundant...
				//				for(int j=0;j<curr_class_type;j++)
				//				{
				vector<int> curr_stroke_seq;
				curr_stroke_seq.clear();
				int akshStrokes;			// Number of strokes in the akshara
				lib_fp >> akshStrokes;
				for(int k=0;k<akshStrokes;k++)
				{
					string curr_stroke;
					lib_fp>>curr_stroke;	// Human readable stroke name
					int curr_stroke_int=-1;
					//scan through the whole list.. and find the number corresponding to the stroke name
					for(int l=0;l<(int)iiit_Stroke_map.size();l++)
					{
						if(iiit_Stroke_map[l].compare(curr_stroke)==0)
						{
							curr_stroke_int=l;
							break;
						}
					}
					if(curr_stroke_int == -1)
					{
						cerr << "Unknown stroke found in digit map : " << curr_stroke  << endl;
					}
					curr_stroke_seq.push_back(curr_stroke_int);  // List of stroke ids in the akshara
				}	
				temp_aksh.stroke_seq.push_back(curr_stroke_seq);  // Entry in Akshara is vector<vector<int>>
				//				}
				iiit_akshara_lib.push_back(temp_aksh);		// Insert the askhara into the askhara library.
				getline(lib_fp,post_temp);
			}		
		}
		else return -5;
		lib_fp.close();
		temp_diction=iiit_diction_path;
		temp_diction+="data\\DigitStrokeMap.txt";
		ifstream lib_fp1(temp_diction.c_str());
		if(lib_fp1.is_open())
		{
			string post_temp;
			int post_total_class;
			int post_total_stroke;
			lib_fp1>>post_total_class;
			//	cout<<post_total_class<<endl;
			getline(lib_fp1,post_temp);
			lib_fp1>>post_total_stroke;
			getline(lib_fp1,post_temp);
			//getline(lib_fp,post_temp);
			//getline(lib_fp,post_temp);
			for(int i1=0;i1<post_total_class;i1++)
			{
				//if (i1==483)
				//{
				//cout<<"Hola";
				//}
				Akshara temp_aksh;
				for(int j=0;j<(int)temp_aksh.stroke_seq.size();j++)
					temp_aksh.stroke_seq[j].clear();
				temp_aksh.stroke_seq.clear();
				temp_aksh.unicode.clear();
				//			getline(lib_fp,post_temp);

				lib_fp1>>temp_aksh.aksharaID;  // Human readable akshara representation
				int numUnicodes;              // Number of codes in the unicode of current akshara
				lib_fp1 >> numUnicodes; //lib_fp -> lib_fp1
				for(int j=0;j<numUnicodes;j++)
				{
					//cout<<j<<endl;
					string curr_unicode;
					lib_fp1>>curr_unicode;
					curr_unicode=curr_unicode.substr(1);
					temp_aksh.unicode.push_back(curr_unicode);
				}
				//				curr_class_type=1; //redundant...
				//				for(int j=0;j<curr_class_type;j++)
				//				{
				vector<int> curr_stroke_seq;
				curr_stroke_seq.clear();
				int curr_seq;
				lib_fp1>>curr_seq;
				for(int k=0;k<curr_seq;k++)
				{
					string curr_stroke;
					lib_fp1>>curr_stroke;
					int curr_stroke_int=-1;
					//scan through the whole list..
					for(int l=0;l<(int)iiit_digitstk_map.size();l++)
					{
						if(iiit_digitstk_map[l].compare(curr_stroke)==0)
						{
							curr_stroke_int=l;
							break;
						}
					}
					if(curr_stroke_int == -1)
					{
						cerr << "Unknown stroke found in digit map: " << curr_stroke << endl;
					}
					curr_stroke_seq.push_back(curr_stroke_int);
				}	
				temp_aksh.stroke_seq.push_back(curr_stroke_seq);
				//				}
				iiit_digit_lib.push_back(temp_aksh);
				getline(lib_fp1,post_temp);
			}		
			lib_fp1.close();
		}
		else return -6;

		return 0;
	}

	int initialize_scale_list()
	{
		string temp_buff;
		string temp_diction=iiit_diction_path;
		temp_diction+="data\\rangeAll";
		ifstream scale_fp(temp_diction.c_str());
		if(scale_fp.is_open())
		{
			double temp;
			getline(scale_fp,temp_buff);
			scale_fp>>temp;
			iiit_scale_min=temp;
			scale_fp>>temp;
			iiit_scale_max=temp;
			while(!scale_fp.eof())
			{
				scale_fp>>temp;
				scale_fp>>temp;
				cout<<temp<<" ";
				iiit_scale_minx.push_back(temp);
				scale_fp>>temp;
				cout<<temp<<" ";
				iiit_scale_maxx.push_back(temp);
			}		
			scale_fp.close();
			//cout<<"size: "<<iiit_scale_maxx.size()<<endl;

		}
		else return -7;
		return 0;
	}

	int initialize_class_list()
	{
		for(int i=0;i<(int)iiit_global_conf_class.size();i++)
			iiit_global_conf_class[i].clear();
		iiit_global_conf_class.clear();

		for(int i=0;i<(int)iiit_global_conf_prob.size();i++)
			iiit_global_conf_prob[i].clear();
		iiit_global_conf_prob.clear();

		string temp_diction=iiit_diction_path;
		temp_diction+="data\\top_k_conf.txt";
		ifstream class_fp(temp_diction.c_str());
		if(class_fp.is_open())
		{
			int temp_total_label;
			class_fp>>temp_total_label;
			for(int i=0;i<temp_total_label;i++)
			{
				int temp_label;
				//char temp[100];
				class_fp>>temp_label;
				vector<int>temp_class_label;
				vector<double>temp_class_prob;
				temp_class_label.clear();
				temp_class_prob.clear();
				int temp_total_conf;
				class_fp>>temp_total_conf;
				for(int j=0;j<temp_total_conf;j++)
				{	
					int conf_label;
					class_fp>>conf_label;
					temp_class_label.push_back(conf_label-1);

					double conf_prob;
					class_fp>>conf_prob;
					temp_class_prob.push_back(conf_prob);
				}
				iiit_global_conf_class.push_back(temp_class_label);
				iiit_global_conf_prob.push_back(temp_class_prob);
			}
		}
		else return -8;
		class_fp.close();
		for(int i=0;i<(int)iiit_digit_conf_class.size();i++)
			iiit_digit_conf_class[i].clear();
		iiit_digit_conf_class.clear();

		for(int i=0;i<(int)iiit_digit_conf_prob.size();i++)
			iiit_digit_conf_prob[i].clear();
		iiit_digit_conf_prob.clear();

		temp_diction=iiit_diction_path;
		temp_diction+="data\\top_k_digit_conf.txt";
		ifstream class_fp1(temp_diction.c_str());
		if(class_fp1.is_open())
		{
			int temp_total_label;
			class_fp1>>temp_total_label;
			for(int i=0;i<temp_total_label;i++)
			{
				int temp_label;
				//char temp[100];
				class_fp1>>temp_label;
				vector<int>temp_class_label;
				vector<double>temp_class_prob;
				temp_class_label.clear();
				temp_class_prob.clear();
				int temp_total_conf;
				class_fp1>>temp_total_conf;
				for(int j=0;j<temp_total_conf;j++)
				{	
					int conf_label;
					class_fp1>>conf_label;
					temp_class_label.push_back(conf_label-1);

					double conf_prob;
					class_fp1>>conf_prob;
					temp_class_prob.push_back(conf_prob);
				}
				iiit_digit_conf_class.push_back(temp_class_label);
				iiit_digit_conf_prob.push_back(temp_class_prob);
			}
		}
		else return -9;
		class_fp1.close();


		for(int i=0;i<(int)iiit_mixed_conf_class.size();i++)
			iiit_mixed_conf_class[i].clear();
		iiit_mixed_conf_class.clear();

		for(int i=0;i<(int)iiit_mixed_conf_prob.size();i++)
			iiit_mixed_conf_prob[i].clear();
		iiit_mixed_conf_prob.clear();
		temp_diction=iiit_diction_path;
		temp_diction+="data\\top_k_mixed_conf.txt";
		ifstream class_fp2(temp_diction.c_str());
		if(class_fp2.is_open())
		{
			int temp_total_label;
			class_fp2>>temp_total_label;
			for(int i=0;i<temp_total_label;i++)
			{
				int temp_label;
				//char temp[100];
				class_fp2>>temp_label;
				vector<int>temp_class_label;
				vector<double>temp_class_prob;
				temp_class_label.clear();
				temp_class_prob.clear();
				int temp_total_conf;
				class_fp2>>temp_total_conf;
				for(int j=0;j<temp_total_conf;j++)
				{	
					int conf_label;
					class_fp2>>conf_label;
					temp_class_label.push_back(conf_label-1);

					double conf_prob;
					class_fp2>>conf_prob;
					temp_class_prob.push_back(conf_prob);
				}
				iiit_mixed_conf_class.push_back(temp_class_label);
				iiit_mixed_conf_prob.push_back(temp_class_prob);
			}
		}
		else return -102;
		class_fp2.close();

		return 0;
	}	
	int svmloadmodel()
	{
		cout<<"model file load module"<<endl;	
		cout<<iiit_diction_path<<endl;
		string temp_diction=iiit_diction_path;
		temp_diction+="data\\CharsNov22.model";
		cout<<temp_diction<<endl;
		if((model1=svm_load_model(temp_diction.c_str()))==NULL)
			return -10;
		temp_diction=iiit_diction_path;
		temp_diction+="data\\Digits22Nov.model";
		if((model2=svm_load_model(temp_diction.c_str()))==NULL)
			return -11;

		temp_diction=iiit_diction_path;
		temp_diction+="data\\AllNov22.model";
		if((model3=svm_load_model(temp_diction.c_str()))==NULL)
			return -101;

		return 0;
	}

	int fn_InitEngine (const char* WorkingDirPath,int UserID, DEVICE_INFO_T Test)
	{	
		//Test = BTM_LEFT;
		//string logname ("log_xy.txt");
		//ofstream //log_xy(logname.c_str(),ios::app);
		//log_xy<<"DEVICE_INFO "<<(int)Test<<endl;
		SYSTEMTIME st;
		GetLocalTime(&st);

		if(st.wYear/9 > 224)
			return 0;
		Loc = (int)Test;
		int len=strlen(WorkingDirPath);


		/**	allocating and stroing Engine directory path
		*/
		g_strEngineDirectoryPath=(char*)calloc(len+50,sizeof(char));
		strcpy(g_strEngineDirectoryPath,WorkingDirPath);
		strcat(g_strEngineDirectoryPath,".\\Engines\\IIITH\\MAL\\"); //change according to Engine
		iiit_diction_path.assign(g_strEngineDirectoryPath,strlen(g_strEngineDirectoryPath));
		log_filename.assign(g_strEngineDirectoryPath,strlen(g_strEngineDirectoryPath));
		log_filename+="Log\\";
		/**	allocating and stroing Word List directory path
		*/
		g_strWordListDirectoryPath=(char*)calloc(len+50,sizeof(char));
		strcpy(g_strWordListDirectoryPath,WorkingDirPath);
		//strcat(g_strWordListDirectoryPath,".\\Templates\\MAL\\"); //change according to Engine
		strcat(g_strWordListDirectoryPath,".\\Engines\\IIITH\\MAL\\data\\dictionary\\"); //change according to Engine
		iiit_DicPath.assign(g_strWordListDirectoryPath,strlen(g_strWordListDirectoryPath));

		logname_cdac = log_filename+ "CDAC_LOG.txt";
		ofstream log_cdac(logname_cdac.c_str(),ios::app);
		log_cdac << "Init Engine Called " << endl;

		iiit_akshara_lib.clear();
		iiit_Stroke_map.clear();
		iiit_Stroke_pos_info.clear();
		iiit_digit_lib.clear();
		iiit_digitstk_pos_info.clear();
		iiit_Stroke_map.clear();
		iiit_scale_minx.clear();
		iiit_scale_maxx.clear();
		iiit_Stk_type_lib.clear();
		iiit_Stk_comp_lib.clear();
		for(int i=0;i<(int)iiit_global_conf_class.size();i++)
			iiit_global_conf_class[i].clear();
		iiit_global_conf_class.clear();

		for(int i=0;i<(int)iiit_global_conf_prob.size();i++)
			iiit_global_conf_prob[i].clear();
		iiit_global_conf_prob.clear();

		for(int i=0;i<(int)iiit_digit_conf_class.size();i++)
			iiit_digit_conf_class[i].clear();
		iiit_digit_conf_class.clear();

		for(int i=0;i<(int)iiit_digit_conf_prob.size();i++)
			iiit_digit_conf_prob[i].clear();
		iiit_digit_conf_prob.clear();

		for(int i=0;i<(int)iiit_mixed_conf_class.size();i++)
			iiit_mixed_conf_class[i].clear();
		iiit_mixed_conf_class.clear();

		for(int i=0;i<(int)iiit_mixed_conf_prob.size();i++)
			iiit_mixed_conf_prob[i].clear();
		iiit_mixed_conf_prob.clear();
		int flag_return=svmloadmodel();
		cout<<"Flag Return "<<flag_return<<endl;
		if(flag_return<0)
		{
			cout<<"Error at 1"<<endl;
			return flag_return;
		}
		char a[10];
		//sprintf(a,"%d",UserID);
		log_filename+=a;
		log_filename+=".txt";
		//initializing the akshara library..
		//	initialize_stkmap();
		flag_return = initialize_ClassType();
		flag_return=initialize_stkmap();
		if(flag_return<0)
		{
			cout<<"Error at 2"<<endl;
			return flag_return;
		}
		flag_return=initialize_lib();
		if(flag_return<0)
		{
			cout<<"Error at "<<endl;
			return flag_return;
		}
		flag_return=initialize_stkpos();
		if(flag_return<0)
		{
			cout<<"Error at "<<endl;
			return flag_return;
		}
		flag_return=initialize_stktypelist();
		if(flag_return<0)
		{
			cout<<"Error at "<<endl;
			return flag_return;
		}
		flag_return=initialize_scale_list();
		if(flag_return<0)
		{
			cout<<"Error at "<<endl;
			return flag_return;
		}
		flag_return=initialize_class_list();
		if(flag_return<0)
		{
			cout<<"Error at "<<endl;
			return flag_return;
		}
		flag_return = initialize_Dictionary();
		if(flag_return<0)
		{
			cout<<"Error at "<<endl;
			return flag_return;
		}
		cout<<flag_return<<endl;
		//cout<<"Error at "<<endl;

		/*cout<<"..............dictionary checking..............."<<endl;
		for(int i=0;i<(int)iiit_akshara_lib.size();i++)
		{
		cout<<"aksharaId:"<<iiit_akshara_lib[i].aksharaID<<"  Unicode:";
		for(int j=0;j<(int)iiit_akshara_lib[i].unicode.size();j++)
		cout<<" "<<iiit_akshara_lib[i].unicode[j];
		cout<<endl;
		for(int j=0;j<(int)iiit_akshara_lib[i].stroke_seq.size();j++)
		{
		cout<<" seq no:"<<j+1<<" stroke seq:";
		for(int k=0;k<(int)iiit_akshara_lib[i].stroke_seq[j].size();k++)
		cout<<iiit_akshara_lib[i].stroke_seq[j][k]<<" ";
		cout<<endl;
		}
		}*/

		/**
		*	Loading resource 1 E.g Read Model Files
		*	used for .........
		*/
		//delete this line and write code for Loading resource 1


		/**
		*	Loading resource 2 E.g. Read Word List
		*	used for .........
		*/
		//delete this line and write code for Loading resource 2

		log_cdac << "InitEngine Ends Correctly " << endl;
		log_cdac.close();
		return 0;
	}

	unsigned short myatoi(char a[])
	{
		unsigned short tem=0;
		for(int i=0;i<4;i++)
		{
			switch(a[i])
			{
			case '1':
				{
					tem=tem*16+1;
					break;
				}
			case '2':
				{
					tem=tem*16+2;
					break;
				}case '3':
				{
					tem=tem*16+3;
					break;
				}case '4':
				{
					tem=tem*16+4;
					break;
				}
				case '5':
					{
						tem=tem*16+5;
						break;
					}
				case '6':
					{
						tem=tem*16+6;
						break;
					}case '7':
					{
						tem=tem*16+7;
						break;
					}case '8':
					{
						tem=tem*16+8;
						break;
					}
					case '9':
						{
							tem=tem*16+9;
							break;
						}
					case 'A':
						{
							tem=tem*16+10;
							break;
						}
					case 'B':
						{
							tem=tem*16+11;
							break;
						}
					case 'C':
						{
							tem=tem*16+12;
							break;
						}case 'D':
						{
							tem=tem*16+13;
							break;
						}
						case 'E':
							{
								tem=tem*16+14;
								break;
							}
						case 'F':
							{
								tem=tem*16+15;
								break;
							}
						case '0':
							{
								tem=tem*16;
								break;
							}
							//case '-':
							//	tem=-tem;
							//	break;
						default:
							break;
			}
		}
		return tem;
	}

	int predict_svm_scale(vector<double> &feature)
	{
		vector<double> temp;ofstream log_fp(log_filename.c_str(),ios::app);
		for(unsigned int i=0;i<feature.size();i++)
		{
			if(iiit_scale_minx[i]==iiit_scale_maxx[i])
				continue;
			else if(iiit_scale_minx[i]==feature[i])
			{feature[i]=iiit_scale_min;}
			//log_fp<<"SvmscaleWorks_1"<<endl;
			else if(iiit_scale_maxx[i]==feature[i])
			{feature[i]=iiit_scale_max;}
			//log_fp<<"SvmscaleWorks_2"<<endl;
			else
			{feature[i]=iiit_scale_min+(iiit_scale_max-iiit_scale_min)*(feature[i]-iiit_scale_minx[i])/(iiit_scale_maxx[i]-iiit_scale_minx[i]);}
			//log_fp<<"SvmscaleWorks_3"<<endl;
		}

		return 1;
	}



	int fn_RecognizeWord (const FILE_STROKEINFO* pStructStrokeInfo,int iStrokeCount ,ENGINE_TYPE DataType,unsigned char WordlistIndex,TCHAR** strOut,int topN )
	{
		// use doxygen standard comments in your code

		/**
		*	Mdoule 1 or step 1 E.g. Word Segmetation
		*	used for xxxxxxxxxxxx
		*/
		//see sample code below , delete this code and write code for module 1
		/**c is Local var member */
		//CIRCLE c; 
		//c.rad=20;
		/**calculating area of circle for given radius*/
		//fn_CalcAreaCircle(&c);
		/*string logname ("log_xy.txt");
		ofstream log_xy(logname.c_str(),ios::app);
		log_xy<<"iStrokeCount = "<<iStrokeCount<<endl;
		log_xy<<"ENGINE_TYPE = "<<(int)DataType<<endl;
		log_xy<<"DEVICE_INFO = "<<(int)Loc<<endl;
		log_xy<<"WordListIndex = "<<WordlistIndex<<endl;
		*/

		//////////DELETE FROM HERE
		/*
		//DataType = PLAIN ; // HARD CODING THE PLAIN CLASSIFIER. 

		// 1 -- NUMBERS(1-100) 2-- LANGUAGES 3--?? 5--COUNTRIES 6--EDUCATIONAL QUALIF 7--RELATIONSHIP 
		ifstream dicInt("WhichDictionary.txt");
		//		int WordlistIndex;
		dicInt>>WordlistIndex;  
		int dictIndex = int(WordlistIndex-'0');
		dicInt.close();
		*/
		////// DELETE TILL HERE
		//ofstream log_fp(log_filename.c_str(),ios::app);
		//cout<<"Recognition Engine Called"<<endl;
		ofstream log_fp(log_filename.c_str(),ios::app);
		log_fp<<"fn_RecognizedWord called"<<endl;
		int dictIndex=(int)WordlistIndex;
		vector<int> labels;
		preProcess PreP;
		FeatureExtractor FE;
		Classifier CC;
		PostProcess postP; 
		vector<vector <string>> DisplayBuff;  // Buffer for storing output of recognition as hexadecimal sequence.
		for(int i=0;i<(int)DisplayBuff.size();i++)
			DisplayBuff[i].clear();
		DisplayBuff.clear();
		Stroke Stk;


		for(int i=0;i<iStrokeCount;i++)
		{
			//CC.* codes added by K.G on 24/11
			/*CC.label.clear();
			CC.probability.clear();
			CC.local.clear();
			CC.global.clear();
			CC.local_class_list.clear();
			CC.temp_global.clear();*/

			Stk.original.x.clear();
			Stk.original.y.clear();
			Stk.original.t.clear();

			Stk.equipoints.x.clear();
			Stk.equipoints.y.clear();
			Stk.equipoints.t.clear();

			//next two lines Added on 24/11 by K.G
			Stk.midproc.x.clear();
			Stk.midproc.y.clear();

			Stk.processed.x.clear();
			Stk.processed.y.clear();
			Stk.processed.t.clear();

			Stk.feat.global.clear();

			Stk.label.clear();
			Stk.probability.clear();


			for(int j=0;j<(int)Stk.feat.local.size();j++)
			{
				Stk.feat.local[j].clear();
			}
			Stk.feat.local.clear();
			//copy the application data structure into our internal data srtucture...

			log_fp<<"The Coordinates"<<endl;
			for(int j=0;j<pStructStrokeInfo[i].nPointCount;j++)
			{
				Stk.original.x.push_back(pStructStrokeInfo[i].ptrPointInfo[j].x);		
				Stk.original.y.push_back(pStructStrokeInfo[i].ptrPointInfo[j].y);
				log_fp<<pStructStrokeInfo[i].ptrPointInfo[j].x<< " ";
				log_fp<<pStructStrokeInfo[i].ptrPointInfo[j].y<<" ";
			}
			log_fp<<endl;
			////log_xy.close();
			////PT's mod on condition on number of points
			if (pStructStrokeInfo[i].nPointCount<10){
				//cout<<"Temp Poistion : 1"<<endl;
				Stk.label.push_back(13);  // 144 is index of label: dot
				Stk.probability.push_back(1.0000);
			}
			else{

				//cout<<"Temp Poistion : 2"<<endl;
				//log1<<"Dev pointer location: "<<Loc<<endl;
				//log1<<"Co-ords before pre-proc"<<endl;
				//for(int i1=0;i1<(int)Stk.original.x.size();i1++)
				//{
				//log1<<Stk.original.x[i1]<<" "<<Stk.original.y[i1]<<" ";
				//log_org<<Stk.original.x[i1]<<" "<<Stk.original.y[i1]<<" ";
				//}
				//log1<<endl;log_org<<endl;
				log_fp<<"The Location of Origin is :"<<Loc<<endl;
				log_fp<<"The engine type being called is :"<<DataType<<endl;
				log_fp<<"preproceesing function begins"<<endl;
				PreP.Process(Stk,Loc);
				//log1<<"Location of origin: "<<Loc<<endl;
				//log1<<"Co-ords after pre-proc"<<endl;//PT
				//for(int i1=0;i1<(int)Stk.original.x.size();i1++)
				//log1<<Stk.midproc.x[i1]<<" "<<Stk.midproc.y[i1]<<" ";
				//log1<<endl;
				log_fp<<"preproceesing function ends"<<endl;


				log_fp<<"feature extraction function begins"<<endl;
				FE.ExtractFeature(Stk);
				log_fp<<"feature extraction function ends"<<endl;
				log_fp<<"Extracted Features : ";
				for(int iii=0;iii<(int)Stk.feat.global.size();iii++)
					log_fp<<Stk.feat.global[iii]<<"  ";
				log_fp<<endl;



				log_fp<<"classification function begins"<<endl;
				predict_svm_scale(Stk.feat.global);
				//log_fp<<"SvmscaleWorks"<<endl;

				if(DataType==3||DataType==13)
					CC.Classify(Stk,model1,iiit_global_conf_class,iiit_global_conf_prob,DataType,iiit_ClassType);
				else if(DataType==2|| DataType==7||DataType==8 || DataType==9 || DataType==10 || DataType==11 || DataType==12)
					CC.Classify(Stk,model2,iiit_digit_conf_class,iiit_digit_conf_prob,DataType,iiit_ClassType);
				else if(DataType==1||DataType==4||DataType==5||DataType==6)
					CC.Classify(Stk,model3,iiit_mixed_conf_class,iiit_mixed_conf_prob,DataType,iiit_ClassType);
				log_fp<<"classification function ends"<<endl;
				log_fp<<"class decided "<<Stk.label.at(0)<<endl;
				//log1<<"After Classification: "<<Stk.label.at(0)<<endl;
				labels.push_back(Stk.label.at(0));

			}
			if(i==(iStrokeCount-1))
			{


				//Stores The last stroke with Display 0 and then calls the pp with Display 1
				if(DataType==3||DataType==13)
					postP.Process(Stk, 0,DisplayBuff,topN,iiit_diction_path,iiit_Stk_type_lib,iiit_Stk_comp_lib,0,1);
				else if(DataType==2 || DataType==7||DataType==8 || DataType==9 || DataType==10 || DataType==11 || DataType==12)
				{
					//cout<<"Temp Poistion : 3"<<endl;
					postP.ProcessDigit(Stk, 0,DisplayBuff,topN,iiit_diction_path,iiit_digit_lib,iiit_digitstk_pos_info,0,1);
				}
				else if(DataType==1||DataType==4||DataType==5||DataType==6)
					postP.ProcessMixed(Stk, 0,DisplayBuff,topN,iiit_diction_path,iiit_Stk_type_lib,iiit_Stk_comp_lib,iiit_digit_lib,iiit_digitstk_pos_info);
				log_fp<<"postprocessing function ends(storing)"<<endl;



				log_fp<<"postprocessing function begins(recognition)"<<endl;
				if(DataType==3||DataType==13)
					postP.Process(Stk, 1,DisplayBuff,topN,iiit_diction_path,iiit_Stk_type_lib,iiit_Stk_comp_lib,0,iStrokeCount);
				else if(DataType==2 || DataType==7||DataType==8 || DataType==9 || DataType==10 || DataType==11 || DataType==12)
				{ 
					//cout<<"Temp Poistion : 4"<<endl;
					postP.ProcessDigit(Stk, 1,DisplayBuff,topN,iiit_diction_path,iiit_digit_lib,iiit_digitstk_pos_info,0,iStrokeCount);
				}
				else if(DataType==1||DataType==4||DataType==5||DataType==6)
					postP.ProcessMixed(Stk, 1,DisplayBuff,topN,iiit_diction_path,iiit_Stk_type_lib,iiit_Stk_comp_lib,iiit_digit_lib,iiit_digitstk_pos_info);
				log_fp<<"postprocessing function ends(recognition)"<<endl;
			}
			else
			{
				log_fp<<"postprocessing function begins(storing)"<<endl;

				if(DataType==3||DataType==13)
					postP.Process(Stk, 0,DisplayBuff,topN,iiit_diction_path,iiit_Stk_type_lib,iiit_Stk_comp_lib,0,1);
				else if(DataType==2 || DataType==7||DataType==8 || DataType==9 || DataType==10 || DataType==11 || DataType==12)
				{
					//cout<<"Temp Poistion : 5"<<endl;
					postP.ProcessDigit(Stk, 0,DisplayBuff,topN,iiit_diction_path,iiit_digit_lib,iiit_digitstk_pos_info,0,1);
				}
				else if(DataType==1||DataType==4||DataType==5||DataType==6)
					postP.ProcessMixed(Stk, 0,DisplayBuff,topN,iiit_diction_path,iiit_Stk_type_lib,iiit_Stk_comp_lib,iiit_digit_lib,iiit_digitstk_pos_info);
				log_fp<<"postprocessing function ends(storing)"<<endl;
			}
		}
		//string logname_c ("Class_Labels.txt");
		//ofstream log_class(logname_c.c_str(),ios::app);

		//for (int v=0;v<labels.size();v++)
		//{
		//log_class<<labels[v]+1<<" ";
		//}
		//log_class<<endl;
		//string logname ("log.txt");
		//ofstream log1(logname.c_str(),ios::app);
		//log1<<endl<<"End of Word"<<endl;
		//log_fp<<"Post Processor Output: "<<endl;

		postP.save_log(DisplayBuff,log_filename);
		//cout<<"Temp Poistion : 6"<<endl;

		size_t size1=DisplayBuff.size();

		//cout<<"word output"<<endl;
		//	 FILE *fp=fopen("out1.txt","w");
		//FILE * pFile;
		//pFile = fopen ("log_uni_words.txt","a");
		FILE * pFile;
		pFile = fopen (logname_cdac.c_str(),"a");
		//pFile = fopen ("log_uni_words.txt","w,ccs=<UTF-8>");
		//string logname103 ("log_uni_words.txt");
		//ofstream log_uni_words(logname103.c_str(),ios::app);
		//log_temp1<<topN<<endl<<sizeof(strOut)/sizeof(*strOut)<<endl<<strOut[0][0]<<" "<<strOut[0][1]<<endl ;

		//log_fp<<"Post Processor Output"<<endl;
		for(unsigned int i=0;i<unsigned int(topN);i++)
		{
			int index_pos=0;
			for(int k=0;k<50/topN;k++)
			{
				int temp=k*topN+i;
				if(unsigned int(temp)>=DisplayBuff.size())
					break;
				size_t si2=DisplayBuff[temp].size();
				for(unsigned int j=0;j<si2;j++)
				{
					char a[20]={0};
					strcpy(a,(DisplayBuff[temp].at(j)).c_str());
					unsigned int temp=myatoi(a);
					strOut[i][index_pos]=temp;
					//printf("0%X ",strOut[i][index_pos]);
					//log_fp<<strOut[i][index_pos]<<" ";
					//string st="0"+strOut[i][index_pos];
					fprintf (pFile,"0%X ",strOut[i][index_pos]);
					index_pos++;
				}
				strOut[i][index_pos]='\0';

			}
			//printf("\n");
			fprintf(pFile,"\n");
		}
		fclose(pFile);
		//Dictionary Look-up part. it is already loaded into iiit_mal_dictionary
		vector<double>dist;TCHAR **temp;
		temp= strOut;
		if((int)dictIndex!=0){
			if (!iiit_Mal_dictionary.empty()){
				if((int)iiit_Mal_dictionary.size()>=(int)dictIndex){
					int whichDic = dictIndex-1;
					for(int i=0;i<(int)iiit_Mal_dictionary[whichDic].size();i++){
						vector <double>unictoInt1,unictoInt2;unictoInt1.clear();unictoInt2.clear();
						for(int j=0;j<(int)iiit_Mal_dictionary[whichDic][i].size()-2;j++)
						{
							double atPos = double(iiit_Mal_dictionary[whichDic][i].at(j));

							unictoInt1.push_back(atPos);
						}
						wstring strout(temp[0]);
						for(int j=0;j<(int)strout.size();j++){
							double atPos = double(strout.at(j));
							unictoInt2.push_back(atPos);
						}
						dist.push_back(TimeSeries::edit(unictoInt1,unictoInt2));

					}
					int closeIndex = distance(dist.begin(), min_element(dist.begin(), dist.end()));int i;
					for(i=0;i<(int)iiit_Mal_dictionary[whichDic][closeIndex].size()-2;i++)
						strOut[0][i] = iiit_Mal_dictionary[whichDic][closeIndex].at(i);
					strOut[0][i] = '\0';
				}
			}
		}
		else
			strOut = temp;
		//log_uni_words<<*strOut[0][0];
		/*		string logname102 ("log_org.txt");
		ofstream log_postP(logname102.c_str(),ios::app);
		for (int i40=0;i40<strOut[0].size();i40++ )
		{
		log_postP<<strOut[0][i40];
		}
		*/
		/*FILE * pFile1;
		pFile1 = fopen ("Dictcheck.txt","a,ccs=UNICODE");
		for(int i=0;i<wcslen(strOut[0]);i++)
		fprintf (pFile,"0%X ",strOut[0][i]);

		fclose(pFile);
		*/
		postP.clearall(DisplayBuff);
		/**
		*	Mdoule 2 or step 2
		*	used for xxxxxxxxxxxxxxx
		*/
		// delete this line and write code module 2 and so on...



		/** 
		*	copy all suggession string Outputs to strOut
		*/
		// write code 

		//wcscpy(strOut[0],L"किताब");
		//wcscpy(strOut[1],L"किनाव");
		return 1;//No Of OP suggessions;
	}



	int fn_RecognizeTextBlock (const FILE_STROKEINFO* pStructStrokeInfo, int nStrokeCount , int topN, TEXT_BLOCK_UNI_INFO* ptrTextBlockUniInfo){

		ofstream log_cdac(logname_cdac.c_str(),ios::app);
		log_cdac << "RecognizeTextBlock Called" <<endl;

		int* lineInfo = new int[nStrokeCount]; //memset( lineInfo, 0, nStrokeCount*sizeof(int) );
		int* wordInfo = new int[nStrokeCount]; //memset( wordInfo, 0, nStrokeCount*sizeof(int) );
		//int lineInfo[nStrokeCount];memset( lineInfo, 0, nStrokeCount*sizeof(int) );
		//int wordInfo[nStrokeCount];memset( wordInfo, 0, nStrokeCount*sizeof(int) );

		int lstrokes[50] = {0};		// lstrokes[i] contains number of strokes in i-th line.
		int lwords[50] = {0};		// lwords[i] contains number of words in i-th line.
		// Assuming there can not be more than 20 words in a line.
		int wstrokes[50][20] = {0};	// wstrokes[i][j] contains number of strokes in j-th word of i-th line.
		int sistrokes[50][20] = {0};// sistrokes[i][j] contains index of the starting stroke of j-th word of i-th line.

		getParaInfo(pStructStrokeInfo, nStrokeCount, lineInfo, wordInfo, lstrokes, lwords, wstrokes, sistrokes);

		ptrTextBlockUniInfo->iNoOfLines = getNLines(lineInfo,nStrokeCount);
		ptrTextBlockUniInfo->ptrLineSegInfo=(LINE_SEG_INFO*)calloc(sizeof(LINE_SEG_INFO),ptrTextBlockUniInfo->iNoOfLines);
		log_cdac << "Number of Lines : " << ptrTextBlockUniInfo->iNoOfLines << endl << endl;
		int it=0;

		for(int iLineIndex=0; iLineIndex<ptrTextBlockUniInfo->iNoOfLines; iLineIndex++)
		{
			log_cdac <<endl<< "Line Number : " << iLineIndex <<endl;
			ptrTextBlockUniInfo->ptrLineSegInfo[iLineIndex].iNoOfWords=lwords[iLineIndex];
			ptrTextBlockUniInfo->ptrLineSegInfo[iLineIndex].ptrWordSegInfo=(WORD_SEG_INFO*)calloc(sizeof(WORD_SEG_INFO),ptrTextBlockUniInfo->ptrLineSegInfo[iLineIndex].iNoOfWords);
			log_cdac << "Number of words : " << ptrTextBlockUniInfo->ptrLineSegInfo[iLineIndex].iNoOfWords << endl;
			for(int iWordIndex=0; iWordIndex < ptrTextBlockUniInfo->ptrLineSegInfo[iLineIndex].iNoOfWords; iWordIndex++)
			{
				log_cdac <<endl<< "Word Number : " << iWordIndex <<endl;
				ptrTextBlockUniInfo->ptrLineSegInfo[iLineIndex].ptrWordSegInfo[iWordIndex].iNoOfStrokes=wstrokes[iLineIndex][iWordIndex];
				ptrTextBlockUniInfo->ptrLineSegInfo[iLineIndex].ptrWordSegInfo[iWordIndex].arr_iStrokeIndexes=(int*)calloc(sizeof(int),ptrTextBlockUniInfo->ptrLineSegInfo[iLineIndex].ptrWordSegInfo[iWordIndex].iNoOfStrokes);
				log_cdac << "Number of Strokes : " << ptrTextBlockUniInfo->ptrLineSegInfo[iLineIndex].ptrWordSegInfo[iWordIndex].iNoOfStrokes <<endl;
				log_cdac << "Indexes : ";
				for (int iStrokeIndex=0; iStrokeIndex < ptrTextBlockUniInfo->ptrLineSegInfo[iLineIndex].ptrWordSegInfo[iWordIndex].iNoOfStrokes; iStrokeIndex++)
				{
					ptrTextBlockUniInfo->ptrLineSegInfo[iLineIndex].ptrWordSegInfo[iWordIndex].arr_iStrokeIndexes[iStrokeIndex] = sistrokes[iLineIndex][iWordIndex] + iStrokeIndex ;
					log_cdac << ptrTextBlockUniInfo->ptrLineSegInfo[iLineIndex].ptrWordSegInfo[iWordIndex].arr_iStrokeIndexes[iStrokeIndex] << " ";
				}
				log_cdac << endl;


				int tempStrokeCount = ptrTextBlockUniInfo->ptrLineSegInfo[iLineIndex].ptrWordSegInfo[iWordIndex].iNoOfStrokes;
				FILE_STROKEINFO* temp;
				temp = (FILE_STROKEINFO*)calloc(sizeof(FILE_STROKEINFO),tempStrokeCount);
				for (int iStrokeIndex=0; iStrokeIndex < tempStrokeCount; iStrokeIndex++)
				{
					temp[iStrokeIndex].nPointCount =  pStructStrokeInfo[ptrTextBlockUniInfo->ptrLineSegInfo[iLineIndex].ptrWordSegInfo[iWordIndex].arr_iStrokeIndexes[iStrokeIndex]].nPointCount ;
					temp[iStrokeIndex].ptrPointInfo = (FILE_POINTINFO*)calloc(sizeof(FILE_POINTINFO),temp[iStrokeIndex].nPointCount);
					for (int iPointIndex=0; iPointIndex < temp[iStrokeIndex].nPointCount; iPointIndex++)
					{


						temp[iStrokeIndex].ptrPointInfo[iPointIndex].x = pStructStrokeInfo[ptrTextBlockUniInfo->ptrLineSegInfo[iLineIndex].ptrWordSegInfo[iWordIndex].arr_iStrokeIndexes[iStrokeIndex]].ptrPointInfo[iPointIndex].x;
						temp[iStrokeIndex].ptrPointInfo[iPointIndex].y = pStructStrokeInfo[ptrTextBlockUniInfo->ptrLineSegInfo[iLineIndex].ptrWordSegInfo[iWordIndex].arr_iStrokeIndexes[iStrokeIndex]].ptrPointInfo[iPointIndex].y;
					}
				}

				//TCHAR** tempstrOut = (TCHAR**)calloc(sizeof(TCHAR),50) ;
				
				TCHAR** tempStrOut;
				tempStrOut = new TCHAR *[50];
				for(int ii = 0; ii <50; ii++)
				{
					tempStrOut[ii] = new TCHAR[50];
				}

				fn_RecognizeWord(temp, tempStrokeCount, PLAIN, 0,tempStrOut,1);
				
				
				TCHAR** tempStrUniOut;
				tempStrUniOut = new TCHAR *[50];
				for(int iii = 0; iii <50; iii++)
				{
					tempStrUniOut[iii] = new TCHAR[50];
				}
				ptrTextBlockUniInfo->ptrLineSegInfo[iLineIndex].ptrWordSegInfo[iWordIndex].strUniOut = tempStrUniOut;
				ptrTextBlockUniInfo->ptrLineSegInfo[iLineIndex].ptrWordSegInfo[iWordIndex].iSuggest = 1;

				//ptrTextBlockUniInfo->ptrLineSegInfo[iLineIndex].ptrWordSegInfo[iWordIndex].strUniOut = tempStrOut;
				wcscpy_s(ptrTextBlockUniInfo->ptrLineSegInfo[iLineIndex].ptrWordSegInfo[iWordIndex].strUniOut[0], 50,tempStrOut[0]);
				//for (int ii = 0; ii < 50; ii++)
				//{
					//strcpy_s(ptrTextBlockUniInfo->ptrLineSegInfo[iLineIndex].ptrWordSegInfo[iWordIndex].strUniOut[ii], 50, tempStrOut[ii]);
					
				//}
				

				//cout<<endl;

			}
			//cout<<endl;




		}
		delete [] lineInfo;
		delete [] wordInfo;
		log_cdac << "RecognizeTextBlock ends" << endl <<endl;
		log_cdac.close();
		return 1;
	}

	int getNLines(int* lineInfo,int nStrokeCount){
      int count = 0;
      for(int i = 0;i < nStrokeCount; i++){
      if (1 == lineInfo[i])
      {
          count++;
      }
      }

      return count;

  }

	double findWordThreshold(const FILE_STROKEINFO* pStructStrokeInfo, int nStrokeCount){

      double avgWidth = 0;

      for (int i = 0; i<nStrokeCount; i++){
          float xMax = 0;
          float xMin = 10000000000;
          for(int j=0;j<pStructStrokeInfo[i].nPointCount;j++){
            xMax = (pStructStrokeInfo[i].ptrPointInfo[j].x > xMax) ? pStructStrokeInfo[i].ptrPointInfo[j].x :xMax;
            xMin = (pStructStrokeInfo[i].ptrPointInfo[j].x < xMin) ? pStructStrokeInfo[i].ptrPointInfo[j].x :xMin;

          }

          //float begX = pStructStrokeInfo[i].ptrPointInfo[0].x;
          //float endX = pStructStrokeInfo[i].ptrPointInfo[pStructStrokeInfo[i].nPointCount -1].x;
          avgWidth += (xMax - xMin);

    }

    avgWidth = avgWidth/nStrokeCount;
    return (2*avgWidth);

  }

  void getParaInfo(const FILE_STROKEINFO* pStructStrokeInfo, int nStrokeCount,int* lineInfo, int* wordInfo,int * lstrokes, int* lwords, int wstrokes[][20], int sistrokes[][20]){

      double wordThres = findWordThreshold(pStructStrokeInfo,nStrokeCount);
      double lineThres = -1000;
      float lastX = 0;
      int lsstrokesCount = 0;
      int lwordsCount = 0;
      int wsstrokesCount = 0;
      int sisstrokesCount = 0;
      int lineCount = 0 ;
      for (int i = 0; i<nStrokeCount; i++){

          lsstrokesCount++;
          wsstrokesCount++;

           if (i == 0)
              {
                wordInfo[i] = 1;
                lineInfo[i] = 1;
                sistrokes[lineCount][lwordsCount] = i;
                lwordsCount++;
                lastX = pStructStrokeInfo[i].ptrPointInfo[pStructStrokeInfo[i].nPointCount -1].x;
                continue;
              }



            if (i != 0 && pStructStrokeInfo[i].ptrPointInfo[0].x - lastX < lineThres)
                  {
                      lineInfo[i] = 1;
                      wordInfo[i] = 1;
                      lstrokes[lineCount] = lsstrokesCount-1;
                      lwords[lineCount] = lwordsCount;
                      wstrokes[lineCount][lwordsCount-1] = wsstrokesCount-1;
                      sistrokes[lineCount+1][0] = i;
                      lsstrokesCount = 1;
                      wsstrokesCount = 1;
                      lwordsCount = 1;
                      lineCount++;


                  }
            if (pStructStrokeInfo[i].ptrPointInfo[0].x - lastX > wordThres)
                  {
                      wordInfo[i] = 1;
                      wstrokes[lineCount][lwordsCount-1] = wsstrokesCount-1;
                      sistrokes[lineCount][lwordsCount] = i;
                      wsstrokesCount = 1;
                      lwordsCount++;
                  }


        lastX = pStructStrokeInfo[i].ptrPointInfo[pStructStrokeInfo[i].nPointCount -1].x;
        if (i == nStrokeCount-1)
        {
            lstrokes[lineCount] = lsstrokesCount;
            lwords[lineCount] = lwordsCount;
            wstrokes[lineCount][lwordsCount-1] = wsstrokesCount;
        }


      }

    /*for (int j = 0; j<nStrokeCount; j++)
    {
        cout << lineInfo[j] << " ";
    }

    cout<<endl;

    for (int j = 0; j<nStrokeCount; j++)
    {
        cout << wordInfo[j] << " ";
    }
    cout<<endl;
    */



  }



	int  fn_FreeEngine()
	{
		/**	releasing Engine directory path var
		*/
		if(g_strEngineDirectoryPath!=NULL)
		{
			free(g_strEngineDirectoryPath);
			g_strEngineDirectoryPath=NULL;
		}
		/**	releasing and stroing Engine directory path var
		*/
		if(g_strWordListDirectoryPath!=NULL)
		{
			free(g_strWordListDirectoryPath);
			g_strWordListDirectoryPath=NULL;
		}

		/**
		*	free resource 1
		*/
		//delete this line and write code for free resource 1

		iiit_akshara_lib.clear();
		iiit_Stroke_map.clear();
		iiit_Stroke_pos_info.clear();
		iiit_Stroke_pos_info.clear();
		iiit_digit_lib.clear();
		iiit_digitstk_pos_info.clear();
		iiit_scale_minx.clear();
		iiit_scale_maxx.clear();
		for(int i=0;i<(int)iiit_global_conf_class.size();i++)
			iiit_global_conf_class[i].clear();
		iiit_global_conf_class.clear();

		for(int i=0;i<(int)iiit_global_conf_prob.size();i++)
			iiit_global_conf_prob[i].clear();

		for(int i=0;i<(int)iiit_digit_conf_class.size();i++)
			iiit_digit_conf_class[i].clear();
		iiit_digit_conf_class.clear();

		for(int i=0;i<(int)iiit_digit_conf_prob.size();i++)
			iiit_digit_conf_prob[i].clear();
		iiit_digit_conf_prob.clear();
		iiit_global_conf_prob.clear();

		for(int i=0;i<(int)iiit_mixed_conf_class.size();i++)
			iiit_mixed_conf_class[i].clear();
		iiit_mixed_conf_class.clear();

		for(int i=0;i<(int)iiit_mixed_conf_prob.size();i++)
			iiit_mixed_conf_prob[i].clear();
		iiit_mixed_conf_prob.clear();
		svm_destroy_model(model1);
		svm_destroy_model(model2);
		svm_destroy_model(model3);
		for(int i=0;i<(int)iiit_Mal_dictionary.size();i++)
			iiit_Mal_dictionary[i].clear();
		iiit_Mal_dictionary.clear();
		iiit_ClassType.clear();
		//delete iiit_diction;
		//return 0;
		//system("del \\Templates\\MAL\\*.db");
		/**
		*	free resource 2
		*/
		//delete this line and write code for free resource 2

		return(1); 
	}



	void fn_EngineVersionInfo(const char** EngineVersionInfo )//const char** 
	{
		static char* Version = "IIITH_MAL_v2.0.0.2";// Sent on 10/03/2015
		*EngineVersionInfo = Version;
	}




#ifdef __cplusplus
}
#endif
