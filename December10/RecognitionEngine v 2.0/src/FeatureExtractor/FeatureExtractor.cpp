
#include <algorithm>
#include <numeric>
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <cmath>
#include <ctime>
#include <cstring>
#include <vector>
using namespace std;
#include "FeatureExtractor.h"

FeatureExtractor::FeatureExtractor()
{
	X.clear();
	Y.clear();
	

	for(int i=0; i<(int) local.size();i++)
	{
		local[i].clear();
	}
	local.clear();
	global.clear();		

}
FeatureExtractor::~FeatureExtractor()
{
	
}

int FeatureExtractor::ExtractFeature(Stroke &Stk)
{
//	FeatureExtractor();
	X.clear();
        Y.clear();


        for(int i=0; i<(int) local.size();i++)
        {
                local[i].clear();
        }
        local.clear();
        global.clear();
	
	Y=Stk.processed.x;				
	X=Stk.processed.y;
	size_t size=X.size();
	/*for(int i=0;i<(int)Stk.processed.x.size();i++)
	{
		//cout<<Stk.processed.x[i]<<" "<<Stk.processed.y[i]<<endl;
	}*/
	feature_minX = *min_element(X.begin(),X.end());
        feature_maxX = *max_element(X.begin(), X.end());
        feature_minY = *min_element(Y.begin(), Y.end());
        feature_maxY = *max_element(Y.begin(), Y.end());		
	feature_meanX=	accumulate(X.begin(), X.end(), 0.0)/size;
	feature_meanY=	accumulate(Y.begin(), Y.end(), 0.0)/size;
	//string logname ("log.txt");
	//ofstream log1(logname.c_str(),ios::app);
	computeGlobalFeat();//log1<<"Global Feat"<<endl;
	Stk.feat.global.insert(Stk.feat.global.end(), global.begin(), global.end());
	eqd(Stk,30);//log1<<"EQD Computation done"<<endl;
	
	//for(int i=0;i<Stk.feat.global.size();i++)
	//	log1<<Stk.feat.global.at(i)<<" ";
	/*for(int i=0;i<Stk.original.x.size();i++)
		log1<<Stk.original.x.at(i)<<" "<<Stk.original.y.at(i)<<" ";*/
	//computeLocalFeat();
	
	/*for(unsigned int i=0;i<Stk.equipoints.x.size();i++)
	{
		global.push_back(Stk.equipoints.x[i]);
		global.push_back(Stk.equipoints.y[i]);
	}
	Stk.feat.local=local;*/
	//Stk.feat.global.=global;
      
	return 1;
}
void FeatureExtractor::eqd(Stroke &Stk,int numpts){
	vector<double> X,Y,X1,Y1;X.clear();Y.clear();
    X=Stk.midproc.x;Y=Stk.midproc.y;
	for(int i=0;i<(int)X.size();i++)
	{X.at(i) = (double)X.at(i);Y.at(i) = (double)Y.at(i);}
	X1.clear();Y1.clear();
    int size = X.size();


	double maxx = (double) *(std::max_element(X.begin(), X.end()));
    double minx = (double) *(std::min_element(X.begin(), X.end()));
    double maxy = (double) *(std::max_element(Y.begin(), Y.end()));
    double miny = (double) *(std::min_element(Y.begin(), Y.end()));
	double scalex = std::max((double) 1, maxx - minx);
    double scaley = std::max((double) 1, maxy - miny);
	double scalexy = std::max(scalex,scaley) / double(1000);
    for (int i = 0; i < size; i++) {
        X[i] /= scalexy;
        Y[i] /= scalexy;
    }

    vector <double> dist(size-1,0),cumdist(size,0);
    vector <int> mask;
    vector<double> tempglobal;
  
    for(int i=0;i<size-1;i++)
        dist.at(i)=distance1(X[i],X[i+1],Y[i],Y[i+1]);
            
    for(int i=1;i<size;i++)  
        cumdist[i] = cumdist[i-1]+dist[i-1];
    
    
    for(int iter=0;iter<int(cumdist.size());iter++){ 
        if (iter+1 != int(cumdist.size()) && (cumdist[iter] == cumdist[iter+1])) {
             cumdist.erase(cumdist.begin()+iter);
             mask.push_back(iter);}
    }
    for(int i=0;i<int(mask.size());i++)
        mask[i] = mask[i]+i;
    
    for(int i=0;i<(int)mask.size();i++){
        X[mask.at(i)]=-1;
        Y[mask.at(i)]=-1;}
    for(int i=0;i<(int)X.size();i++){
        if (X[i]==-1) {
        X.erase(X.begin()+i);
        Y.erase(Y.begin()+i);
        i--;}}
    
    vector <double> diststeps;diststeps.clear();
    
    for(int i=0;i<numpts;i++)
        diststeps.push_back(double(i)*cumdist.back()/(numpts-1));
    
    vector <double> eqdx,eqdy;double temp1=0,temp2=0;
	//vector<double> X,Y,X1,Y1;X.clear();Y.clear();//string logname ("log.txt");
	//ofstream log1(logname.c_str(),ios::app);
    eqdx.clear(); eqdy.clear();//Stk.equipoints.x.clear();Stk.equipoints.y.clear();
	//log1<<"Equipoints final: "<<endl;
    for(int ii=0;ii<numpts;ii++){
        temp1 = lininterp(cumdist,X,diststeps[ii]);
        temp2 = lininterp(cumdist,Y,diststeps[ii]);
        //.push_back(temp1);eqdy.push_back(temp2);
        //Stk.equipoints.x.push_back(temp1);
        //Stk.equipoints.y.push_back(temp2);
		//log1<<ii<<" "<<temp1<<" "<<temp2<<endl;
		//log1<<temp1<<" "<<temp2<<" ";
        //Stk.feat.global.push_back(temp1);
        //Stk.feat.global.push_back(temp2);
		eqdx.push_back(temp1);
        eqdy.push_back(temp2);
    }
	/*for(int ii=0;ii<Stk.equipoints.x.size();ii++){
		eqdx.push_back(Stk.equipoints.x.at(ii));
		eqdy.push_back(Stk.equipoints.y.at(ii));
	}*/


	maxx = (double) *(std::max_element(eqdx.begin(), eqdx.end()));
    minx = (double) *(std::min_element(eqdx.begin(), eqdx.end()));
    maxy = (double) *(std::max_element(eqdy.begin(), eqdy.end()));
    miny = (double) *(std::min_element(eqdy.begin(), eqdy.end()));

    vector<double> tt1;
    vector<double> tt2;
    tt1.clear();
    tt2.clear();
    for (int i = 0; i < (int) eqdx.size(); i++) {
        tt1.push_back(eqdx.at(i) - minx);
        tt2.push_back(eqdy.at(i) - miny);
    }
    scalex = std::max((double) 1, maxx - minx);
    scaley = std::max((double) 1, maxy - miny);
	scalexy = std::max(scalex,scaley);
    for (int i = 0; i < (int) tt1.size(); i++) {
        X1.push_back(tt1.at(i) / scalexy);
        Y1.push_back(tt2.at(i) / scalexy);
    }

	for(int i=0;i<(int)X1.size();i++){
		Stk.feat.global.push_back(X1.at(i));
		Stk.feat.global.push_back(Y1.at(i));
	}

    //cout<<"max is"<<*(std::max_element(eqdx.begin(),eqdx.end()));
            //log1<<endl;
   //log1.close();
}

inline double FeatureExtractor:: distance1 (double a,double b,double c,double d)
{
return  sqrt((a-b)*(a-b) + (c-d)*(c-d));
    //return dbt;
}

double FeatureExtractor::lininterp(vector<double> x,vector<double> y1,double t)
{
    
    /* number of elements in the array */
    //K.G static const int count = int(x.size());
	int count = int(x.size());
	int temp= count;
    int i;
    double dx, dy1,tx;
	//string logname ("log.txt");
	//ofstream log1(logname.c_str(),ios::app);
    /* find i, such that xs[i] <= x < xs[i+1] */
    for (i = 0; i < count-1; i++) {
        if (x[i+1] >= t) {
            break;
        }
    }
	if(i==temp-1)
	{tx = y1[temp-1];}
	//log1<<"In the first if";}
	else
	{
    dx = x[i+1] - x[i];
    dy1 = y1[i+1] - y1[i];
    tx = y1[i] + (t - x[i]) * dy1 / (dx+0.000001);
	}
	
    return tx;
    
}

int FeatureExtractor::computeLocalFeat(void)
{
	size_t size=X.size();
	for(unsigned int i=1;i<size-1;i++)
	{
		double c = computeCurvature(i,i-1,i+1);
                double t = computeTangent(i,i-1,i+1);
                double h = computeHeight(i);
		vector <double> temp;
		
		temp.push_back(c);
		temp.push_back(t);
		temp.push_back(h);
		
		local.push_back(temp);
	}

	return(1);
}

int FeatureExtractor::computeGlobalFeat(void)
{
	double globalcurvature=computeGlobalcurvature();
	double dir=direction(); 
	vector<int> temp(gridoccupancy());

	//vector<double> fourier_vector(Fourier_vector(int(X.size()),10,iiit_diction_path));
	double len=lengthStk();
	global.push_back(computemoment_x(0));	
	global.push_back(computemoment_x(2));	
	global.push_back(computemoment_x(3));	
	global.push_back(computemoment_x(4));	
	global.push_back(computemoment_y(0));	
	global.push_back(computemoment_y(2));	
	global.push_back(computemoment_y(3));	
	global.push_back(computemoment_y(4));
	global.push_back(len);
	global.push_back(globalcurvature);
	global.push_back(dir);
	for(int i=0;i<25;i++)
	{
		global.push_back(temp[i]);
	}
	/*for(int i=0;i<(int)fourier_vector.size();i++)
	{
		global.push_back(fourier_vector[i]);
	}*/
	return(1);
}

double  FeatureExtractor::computeGlobalcurvature(void)
{
        double curvature=0;
	size_t size=X.size();
        for(unsigned int i=1;i<(size-1);i++)
        {
                curvature+=computeCurvature(i,i-1,i+1);
        }
        return curvature/size;
}

double FeatureExtractor::direction(void)
{
        double x1,x2,y1,y2;
        x1=X[0];
        y1=Y[0];
	size_t size=X.size();
        x2=X[size-1];
        y2=Y[size-1];
        if(x1==x2)
        {
                if(y1<=y2)
                        return 3.1416/2;
                else
                        return (3*3.1416)/2;
        }
        else if(y1==y2)
        {
                if(x1<=x2)
                        return 0;
                else
                        return 3.1416;
        }
        else
        {
                if(x1 < x2)
                        return atan( (y2-y1)/(x2-x1));
                else
                        return ( 3.1416 +  atan( (y2-y1)/(x2-x1)) );
        }
}

double  FeatureExtractor::computemoment_x(int n)
{
	size_t size=X.size();
        if(n==0)
                return feature_meanX;
        else if(n==1)
                return 0;
        else
        {
                double moment=0;
                for(unsigned int i=0;i<size;i++)
                {
                        moment+=pow(X[i]-feature_meanX,n);
                }
                moment/=size;
        return moment;
        }
}


double  FeatureExtractor::computemoment_y(int n)
{
	size_t size=Y.size();
        if(n==0)
                return feature_meanY;
        else if(n==1)
                return 0;
        else
        {
                double moment=0;
                for(unsigned int i=0;i<size;i++)
                {
                        moment+=pow(Y[i]-feature_meanY,n);
                }
                moment/=size;
        return moment;
        }
}

vector<int>FeatureExtractor::gridoccupancy(void)
{
	
	double scale_x=(feature_maxX-feature_minX+2)/5.0;
	double scale_y=( feature_maxY-feature_minY+2)/5.0;
	vector<int> gridvector(25,0);
	size_t size=X.size();
	for(unsigned int i=0;i<size;i++)
	{
		int posx=(int)((X[i]-feature_minX+1)/scale_x);
		int posy=(int)((Y[i]-feature_minY+1)/scale_y);
		gridvector[posy*5+posx]++;
	}
	//int count=accumulate(gridvector.begin(),gridvector.end(),0);
	return gridvector;
}

double FeatureExtractor::lengthStk(void)
{
        double len=0;
	size_t size=X.size();
        for(unsigned int i=1;i<size;i++)
        {
                len+=sqrt((X[i]-X[i-1])*(X[i]-X[i-1])+(Y[i]-Y[i-1])*(Y[i]-Y[i-1]));
        }

        return(len);
}


double FeatureExtractor::computeCurvature(int pt1, int pt2, int pt3)
{
    double xd, yd;
    double sl, sr, s ;
    double xx, yy;
    double x1, y1;
    double x2, y2;


    xx = (X[pt1]);
    yy = (Y[pt1]);
    x1 = (X[pt2]);
    y1 = (Y[pt2]);
    x2 = (X[pt3]);
    y2 = (Y[pt3]);

    xd = x1 - xx ;
    yd = y1 - yy ;

    if (xd == 0)
    {
        if (yd >= 0) sl = 90 ; else sl = 270 ;
    }
    else
    {
        sl =(atan ( abs(yd) / (double)(abs(xd)) ) * 180 / 3.1416);
        if ((xd < 0) && (yd >= 0))
            sl = 180 - sl ;
        else if ((xd > 0) && (yd < 0))
            sl = 360 - sl ;
        else if ((xd < 0) && (yd < 0))
            sl += 180 ;
    }
    xd = x2 - xx ;
    yd = y2 - yy ;

    //cout<<xd<<" "<<yd<<" ";
    if (xd == 0)
    {
        if (yd >= 0) sr = 90 ; else sr = 270 ;
    }
    else
    {
        sr = (atan ( abs(yd) / (double)(abs(xd)) ) * 180 / 3.1416);
        if ((xd < 0) && (yd >= 0))
            sr = 180 - sr ;
        else if ((xd > 0) && (yd < 0))
            sr = 360 - sr ;
        else if ((xd < 0) && (yd < 0))
            sr += 180 ;
    }
    s = sl - sr ;
    if (s <= 0) s += 360 ;

//      cout<<sl<<" "<<sr<<endl;
    return ( s ) ;
}


double FeatureExtractor::computeTangent(int pt1, int pt2, int pt3)
{
    double xd, yd;
    double sl, sr, s ;
    double xx, yy;
    double x1, y1;
    double x2, y2;

    xx = X[pt1];
    yy = Y[pt1];
    x1 = X[pt2];
    y1 = Y[pt2];
    x2 = X[pt3];
    y2 = Y[pt3];

    xd = x1 - xx ;
    yd = y1 - yy ;
    if (xd == 0)
    {
             if(yd >= 0) sl = 90 ; else sl = 270 ;
    }
    else
    {
        sl = (atan ( abs(yd) / (double)(abs(xd)) ) * 180 / 3.1416);
        if ((xd < 0) && (yd >= 0))
            sl = 180 - sl ;
        else if ((xd > 0) && (yd < 0))
            sl = 360 - sl ;
        else if ((xd < 0) && (yd < 0))
            sl += 180 ;
    }
    xd = x2 - xx ;
    yd = y2 - yy ;
    if (xd == 0)
    {
        if (yd >= 0) sr = 90 ; else sr = 270 ;
    }
    else
    {
        sr = (atan ( abs(yd) / (double)(abs(xd)) ) * 180 / 3.1416);
	if ((xd < 0) && (yd >= 0))
            sr = 180 - sr ;
        else if ((xd > 0) && (yd < 0))
            sr = 360 - sr ;
        else if ((xd < 0) && (yd < 0))
            sr += 180 ;
    }
    s = (sl + sr)/2 ;
    return ( s ) ;
}

double FeatureExtractor::computeHeight(int pos)
{
    double yy = Y[pos];
    double mY = (*min_element(Y.begin(), Y.end()));
    return(yy-mY);

}



void FeatureExtractor::resample_eq(vector<double>& Xp,vector<double>& Yp,int w)
{
	 vector<double>x;
        x.clear();
        vector<double>y;
        y.clear();
        size_t numPoints;
        numPoints=X.size();
        for(int i=0;i<(int)X.size();i++)
        {
                x.push_back(X[i]);
                y.push_back(Y[i]);
        }

        Xp[0]=x[0];
        Yp[0]=y[0];
        vector<double> L(numPoints,0);
        L[0]=0;
        for(unsigned int i=1;i<numPoints;i++)
        {
                L[i]=L[i-1]+sqrt(   (x[i]-x[i-1])*(x[i]-x[i-1]) + (y[i]-y[i-1])*(y[i]-y[i-1]) );
        }
        double a=L[numPoints-1]/(w);
        double C;
        int j=1;
        for(int p=1;p<w;p++)
        {
                while(L[j]<(p*a))
                {
                        j++;
                }
                C=(p*a - L[j-1])/(L[j] -L[j-1]);
                Xp[p]=x[j-1]+(x[j]- x[j-1])*C;
                Yp[p]=y[j-1]+(y[j]-y[j-1])*C;

        }
        Xp[w]=x[numPoints-1];
        Yp[w]=y[numPoints-1];
}

