import pdb
import matplotlib.pyplot as plt
import numpy as np
original_points=[]
feat_points=[]
with open('log_org.txt','r',encoding='utf-8') as f:
    for line in f:
        #line=line.strip('\n')
       # pdb.set_trace();
        if line=="****":
            continue
        else:
            points=line.strip('\n').split()
           # pdb.set_trace()
            points=[float(i) for i in points]
            xpoints=points[0::2]
            ypoints=points[1::2]
            original_points.append((xpoints,ypoints))
            
with open('log_feat.txt','r') as f:
    for line in f:
        if line=="****":
            continue
        else:
            points=[float(i) for i in line.strip('\n').split()]
            #pdb.set_trace();
            points=points[36::]
           # pdb.set_trace();
            xpoints=points[0::2]
            ypoints=points[1::2]
            feat_points.append((xpoints,ypoints))

for count,o in enumerate(original_points):
    plt.figure()
    plt.subplot(1,2,1)
    plt.plot(o[0],o[1])
    plt.title("original")
    plt.subplot(1,2,2)
    plt.plot(feat_points[count][0][0],feat_points[count][1][0],'*',feat_points[count][0],feat_points[count][1])
    plt.title("Feature")
    plt.show()
    plt.close();
